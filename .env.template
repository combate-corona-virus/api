#!/bin/bash

# =============================================================
# LARAVEL
# =============================================================

APP_NAME=Covid
#local, testing, develop, stage, master, production
APP_ENV=${APP_ENV}
# Descomentar linha abaixo apenas em ambiente de desenvolvimento e apenas caso queira ver a tela de exceção do Laravel
# APP_EXCEPTIONS=showOriginal
APP_KEY=base64:uf1PWLMEfUIwAfXxYMh5wbW3mQ8b9iBlgvT63eEnXB4=
APP_DEBUG=false
APP_WEB_PATH=${APP_WEB_PATH}

SERVER_NAME=${SERVER_NAME}
APP_URL=http://${SERVER_NAME}

DB_CONNECTION=pgsql
DB_HOST=${DB_HOST}
DB_PORT=${DB_PORT}
DB_DATABASE=${DB_DATABASE}
DB_USERNAME=${DB_USERNAME}
# Usar senhas sem espaço
DB_PASSWORD=${DB_PASSWORD}

GOOGLE_MAPS_BASE_URL="https://maps.googleapis.com/maps/api/geocode/json"
GOOGLE_MAPS_API_KEY="AIzaSyA5G-a_pYAMT6K6k3cZa_0hMitiLgxBpnE"

SALT_LEFT=sd5f4
SALT_RIGHT=63ik58

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync
SENTRY_DSN=https://123abc:123abc@sentry.io/123456

MAILGUN_DOMAIN=${MAILGUN_DOMAIN}
MAILGUN_SECRET=${MAILGUN_SECRET}
MAIL_FROM_ADDRESS=${MAIL_FROM_ADDRESS}
MAIL_FROM_NAME=${MAIL_FROM_NAME}
MAIL_DRIVER=${MAIL_DRIVER}
MAIL_TEST_ADDRESS=${MAIL_TEST_ADDRESS}
MAIL_TEST_NAME=${MAIL_TEST_NAME}

# As infos do pusher devem ser parametrizadas no .env de acordo com o ambiente de desenvolvimento (dev,stage ou production)
# Essas infos podem ser encontradas no dashboard do Pusher
BROADCAST_DRIVER=${BROADCAST_DRIVER}
PUSHER_APP_ID=${PUSHER_APP_ID}
PUSHER_APP_KEY=${PUSHER_APP_KEY}
PUSHER_APP_SECRET=${PUSHER_APP_SECRET}
PUSHER_APP_CLUSTER=${PUSHER_APP_CLUSTER}

SLACK_DEFAULT_CHANNEL="${SLACK_DEFAULT_CHANNEL}"
SLACK_WEBHOOK="${SLACK_WEBHOOK}"


#==============================================================
#CONFIGURAÇÕES S3 AMAZON
#==============================================================
#local ou s3 para dev/stage/master/prod
APP_STORAGE_PREFIX=${APP_STORAGE_PREFIX}
FILESYSTEM_DRIVER=${FILESYSTEM_DRIVER}
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}
AWS_BUCKET=${AWS_BUCKET}

#==============================================================
#CONFIGURAÇÕES LIGHTHOUSE
#==============================================================
LIGHTHOUSE_CACHE_ENABLE=${LIGHTHOUSE_CACHE_ENABLE}

# =============================================================
# LARADOCK
# =============================================================
# DOCKER_HOST_IP=192.168.3.15
DOCKER_HOST_IP=10.0.75.1

#==============================================================
#CONFIGURAÇÕES FEATURES
#==============================================================
FEATURE_ENABLE_IMPORT_BOLETIM_EPIDEMIOLOGICO=false

###########################################################
###################### General Setup ######################
###########################################################

### Paths #################################################

# Point to the path of your applications code on your host
APP_CODE_PATH_HOST=./

# Point to where the `APP_CODE_PATH_HOST` should be in the container. You may add flags to the path `:cached`, `:delegated`. When using Docker Sync add `:nocopy`
APP_CODE_PATH_CONTAINER=/var/www:cached

# Choose storage path on your machine. For all storage systems
DATA_PATH_HOST=${DATA_PATH_HOST}

### Drivers ################################################

# All volumes driver
VOLUMES_DRIVER=local

# All Networks driver
NETWORKS_DRIVER=bridge

### Docker compose files ##################################

# Select which docker-compose files to include. If using docker-sync append `:docker-compose.sync.yml` at the end
COMPOSE_FILE=docker-compose.yml

# Change the separator from : to ; on Windows
COMPOSE_PATH_SEPARATOR=:

# Define the prefix of container names. This is useful if you have multiple projects that use laradock to have seperate containers per project.
COMPOSE_PROJECT_NAME=${COMPOSE_PROJECT_NAME}
COMPOSE_NETWORK_NAME=${COMPOSE_NETWORK_NAME}

### PHP Version ###########################################

# Select a PHP version of the Workspace and PHP-FPM containers (Does not apply to HHVM). Accepted values: 7.2 - 7.1 - 7.0 - 5.6
PHP_VERSION=7.2

### Phalcon Version ###########################################

# Select a Phalcon version of the Workspace and PHP-FPM containers (Does not apply to HHVM). Accepted values: 3.4.0+
PHALCON_VERSION=3.4.1

### PHP Interpreter #######################################

# Select the PHP Interpreter. Accepted values: hhvm - php-fpm
PHP_INTERPRETER=php-fpm

### Remote Interpreter ####################################

# Choose a Remote Interpreter entry matching name. Default is `laradock`
PHP_IDE_CONFIG=serverName=laradock

### Windows Path ##########################################

# A fix for Windows users, to ensure the application path works
COMPOSE_CONVERT_WINDOWS_PATHS=1

### Environment ###########################################

# If you need to change the sources (i.e. to China), set CHANGE_SOURCE to true
CHANGE_SOURCE=false

### Docker Sync ###########################################

# If you are using Docker Sync. For `osx` use 'native_osx', for `windows` use 'unison', for `linux` docker-sync is not required
DOCKER_SYNC_STRATEGY=native_osx

###########################################################
################ Containers Customization #################
###########################################################

### WORKSPACE #############################################

WORKSPACE_COMPOSER_GLOBAL_INSTALL=false
WORKSPACE_COMPOSER_REPO_PACKAGIST=
WORKSPACE_INSTALL_NODE=false
WORKSPACE_NODE_VERSION=node
WORKSPACE_NPM_REGISTRY=
WORKSPACE_INSTALL_YARN=false
WORKSPACE_INSTALL_PHPREDIS=false
WORKSPACE_INSTALL_WORKSPACE_SSH=false
WORKSPACE_INSTALL_XDEBUG=${INSTALL_XDEBUG}
WORKSPACE_INSTALL_LARAVEL_ENVOY=false
WORKSPACE_INSTALL_LARAVEL_INSTALLER=false
WORKSPACE_INSTALL_IMAGEMAGICK=true
WORKSPACE_PUID=1000
WORKSPACE_PGID=1000
WORKSPACE_TIMEZONE=UTC-3
WORKSPACE_SSH_PORT=2222

### PHP_FPM ###############################################

PHP_FPM_INSTALL_ZIP_ARCHIVE=true
PHP_FPM_INSTALL_INTL=true
PHP_FPM_INSTALL_IMAGEMAGICK=true
PHP_FPM_INSTALL_IMAGE_OPTIMIZERS=true
PHP_FPM_INSTALL_PHPREDIS=false
PHP_FPM_INSTALL_WKHTMLTOPDF=true
PHP_FPM_INSTALL_XDEBUG=${INSTALL_XDEBUG}
PHP_FPM_INSTALL_GD_ARCHIVE=true
PHP_FPM_INSTALL_XML_ARCHIVE=true
PHP_FPM_FAKETIME=-0
PHP_FPM_INSTALL_BCMATH=true
PHP_FPM_INSTALL_OPCACHE=true

### PHP_WORKER ############################################

PHP_WORKER_INSTALL_PGSQL=true
PHP_WORKER_PUID=1000
PHP_WORKER_PGID=1000
PHP_WORKER_INSTALL_BCMATH=true

### NGINX #################################################

NGINX_HOST_HTTP_PORT=80
NGINX_HOST_HTTPS_PORT=443
NGINX_HOST_LOG_PATH=/var/log/nginx
NGINX_SITES_PATH=./docker/nginx/sites/
NGINX_PHP_UPSTREAM_CONTAINER=php-fpm
NGINX_PHP_UPSTREAM_PORT=9000
NGINX_SSL_PATH=./docker/nginx/ssl/

### REDIS #################################################

REDIS_PORT=6379

### POSTGRES ##############################################

POSTGRES_DB=${DB_DATABASE}
POSTGRES_USER=${DB_USERNAME}
POSTGRES_PASSWORD=${DB_PASSWORD}
POSTGRES_PORT=${DB_PORT}
POSTGRES_ENTRYPOINT_INITDB=./postgres/docker-entrypoint-initdb.d

### LARAVEL ECHO SERVER ###################################

# LARAVEL_ECHO_SERVER_PORT=6001
