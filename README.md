[![pipeline status](https://gitlab.com/combate-corona-virus/api/badges/develop/pipeline.svg)](https://gitlab.com/combate-corona-virus/api/commits/develop)

# API Covid

## Instalação

```bash
git clone git@gitlab.com:combate-corona-virus/api.git /var/www/api-covid
cd /var/www/api-covid

docker-compose up --build
lcomposer install
lcomposer dump-autoload -o

lartisan migrate
```

## Guidelines

### Coding standards - Padrão de escrita de código

O padrão adotado tem como base a normatização de padrões de codificação [PHP Standard Recommendation - PSR](https://www.php-fig.org) - Mais precisamente o [PSR2](https://www.php-fig.org/psr/psr-2/) que diz respeito ao "Coding Style".

Foram feitas algumas pesquisas e encontramos plugins e ferramentas que acreditamos ajudam a seguir essas diretrizes.

Os pacotes composer encontrados foram `friendsofphp/php-cs-fixer`, `squizlabs/php_codesniffer`, `jakub-onderka/php-parallel-lint` e `phpstan/phpstan` que fazem a verificação do código para encontrar possíveis falhas de code style, sintaxe, uso de classes/métodos não existentes.

Todos são rodados automaticamente pelo nosso CI, mas alguns podem ser rodados localmente pelos plugins [phpcs](https://marketplace.visualstudio.com/items?itemName=ikappas.phpcs) e [php cs fixer](https://marketplace.visualstudio.com/items?itemName=junstyle.php-cs-fixer) ou pelo comando `lcomposer ci` que roda todas as verificações.

---

### CI/CD Validações

#### Para simular o que o nosso gitlab ci valida, basta rodar o comando `lcomposer ci`

#### Caso o CI tenha quebrado o pipeline no job `code-quality` tenha certeza de ter rodado o comando `lcomposer fix` antes de enviar o MR

A definição de build pode ser consultada no arquivo .gitlab-ci.yaml.

Não irá passar no build e consequentemente não será mergeado o código que não esteja de acordo com a nossa guideline. Se tiver alguma dúvida pode entrar no nosso canal [#covid](https://institutosoma.slack.com/messages/GGX0JHGSU) do slack e fazer qualquer pergunta.

---

### Testes

Vamos seguir o padrão de escrita de testes já estabelecido pelo [Laravel](https://laravel.com/docs/5.8/testing).

Como rodar os testes:

```bash
docker-compose exec worspace composer test
ou lcomposer test
```

---

### Extensões VS Code Sugeridas

#### Nota: É necessário ter a dependência php-codesniffer instalada no pc pra rodar as extensões que verificam a qualidade do código, para isso rode `sudo apt-get install php-codesniffer`

- [Docker](https://marketplace.visualstudio.com/items?itemName=PeterJausovec.vscode-docker)

- [Docker Linter](https://marketplace.visualstudio.com/items?itemName=henriiik.docker-linter)

- [GitLab CI Validator](https://marketplace.visualstudio.com/items?itemName=cstuder.gitlab-ci-validator)

Configuração:

```bash
"gitlab-ci-validator.gitLabURL": "https://gitlab.com/combate-corona-virus/api"
```

- [Laravel Blade Snippets](https://marketplace.visualstudio.com/items?itemName=onecentlin.laravel-blade) - Autocomplete e snippets

- [Laravel Helpers](https://marketplace.visualstudio.com/items?itemName=rafa-acioly.laravel-helpers) - Autocomplete

- [phpcs](https://marketplace.visualstudio.com/items?itemName=ikappas.phpcs) - Executa o linter no código durante o desenvolvimento baseado nas regras estabelecidas no arquivo `php_cs.dist`

Configuração:

```bash
"phpcs.standard": "PSR2",
"phpcs.executablePath": "vendor/bin/phpcs",
"phpcs.autoConfigSearch": true,
"phpcs.ignorePatterns": ["database/migrations", "bootstrap/cache", "storage", "vendor"],
```

- [php cs fixer](https://marketplace.visualstudio.com/items?itemName=junstyle.php-cs-fixer) - Ajusta o código automaticamente assim que salva o arquivo de acordo com as diretrizes estabelecidas no arquivo `php_cs.dist`

Configuração:

```bash
"php-cs-fixer.config": ".php_cs;.php_cs.dist",
"php-cs-fixer.executablePath": "${extensionPath}/php-cs-fixer.phar",
"php-cs-fixer.onsave": true,
```

- [PHP Intelephense](https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client) - Autocomplete, analise estática do código e suporte pra navegação de referências de métodos em outras classes e etc

- [Prettify JSON](https://marketplace.visualstudio.com/items?itemName=mohsen1.prettify-json) - Formatação de arquivos json

- [GraphQL for VSCode](https://marketplace.visualstudio.com/items?itemName=kumar-harsh.graphql-for-vscode)

---

### Extensões VS Code Obrigatórias

- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens) - Facilidades do git para merge, commit, show changes, history, blame e etc...

- [PHP Debug](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug) - Debugar aplicações php

 Setup:

```bash
alterar no .env DOCKER_HOST_IP="ip da sua maquina"
cd /var/www/api-covid && docker-compose down && docker-compose up
```

---
