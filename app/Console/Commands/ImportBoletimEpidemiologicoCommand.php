<?php

namespace App\Console\Commands;

use App\Imports\ImportBoletimEpidemiologico;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log as Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ImportBoletimEpidemiologicoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:boletim-epidemiologico';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa dados do Brasil.IO referentes ao covid19';

    private $httpClient;

    private const IMPORT_URL = 'https://brasil.io/dataset/covid19/caso?format=csv';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        $this->httpClient = new \GuzzleHttp\Client();

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $downloadInfo = $this->download();
        $path = $downloadInfo['path'];

        $this->debugInfo("arquivo $path recuperado");

        $this->debugInfo("iniciando importação do arquivo $path");

        Excel::import(new ImportBoletimEpidemiologico(), $path);

        $this->debugInfo('importação finalizada');

        Storage::disk('local')->delete($path);
    }

    private function download()
    {
        $this->debugInfo('procurando url para fazer o download do arquivo');

        $url = $this->tryGetDownloadUrl();

        $path = null;
        if ($url) {
            $path = $this->getDownloadSavePath($url);

            // faz o download e salva o arquivo no disco
            $this->httpClient->get($url, ['sink' => $path]);
        }

        return [
            'path' => $path,
            'url' => $url,
        ];
    }

    private function getDownloadSavePath($url)
    {
        $path = storage_path('app/tmp/covid.csv');

        return $path;
    }

    private function tryGetDownloadUrl()
    {
        $validUrl = null;

        $urlTest = self::IMPORT_URL;

        // verifica se é uma url válida
        try {
            $this->httpClient->request('GET', $urlTest, ['verify' => true]);

            $validUrl = $urlTest;

            $this->debugInfo("url válida encontrada: $urlTest");
        } catch (\Exception $ex) {
            $this->debugInfo("url inválida: $urlTest");
        }

        if (!$validUrl) {
            $msg = 'Não foi possível determinar a url para download.';

            $this->debugInfo($msg);
        }

        return $validUrl;
    }

    private function debugInfo($message)
    {
        $this->info($message);
        Log::debug($message);
    }
}
