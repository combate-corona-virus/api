<?php

namespace App\Console;

use App\Helpers\FeaturesHelper;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    private $featuresHelper;

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ImportBoletimEpidemiologicoCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     */
    protected function schedule(Schedule $schedule)
    {
        $featuresHelper = \App::make(FeaturesHelper::class);
        if ($featuresHelper->canImportBoletimEpidemiologico()) {
            $schedule->command('import:boletim-epidemiologico')->dailyAt('11:01')->evenInMaintenanceMode()->withoutOverlapping();
        }
    }

    /**
     * Register the commands for the application.
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
