<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ArquivoBasePath extends Enum
{
    const DEFAULT = 'arquivos';
    const IMAGEM_ORIGINAL = 'arquivos/imagens/original';
    const IMAGEM_LOGO = 'arquivos/imagens/logo';
    const IMAGEM_800X600 = 'arquivos/imagens/800x600';
}
