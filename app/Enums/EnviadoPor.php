<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class EnviadoPor extends Enum
{
    const PREFEITURA = 'PREFEITURA';
    const MUNICIPE = 'MUNICIPE';
}
