<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

// Enum pra facilitar/normatizar a procura por nomes das geocamadas
final class NomeGeocamada extends Enum
{
    //pai de todas
    const PACIENTES = 'pacientes';

    //pai
    const AGENTE_PATOGENO = 'agente_patogeno';

    const CONFIRMADO = 'confirmado';
    const SUSPEITO_EM_ANALISE = 'suspeito_em_analise';
    const DESCARTADO_OUTROS = 'descartado_outros';

    //pai
    const CONDICAO_DO_PACIENTE = 'condição_do_paciente';

    const ALTA_LIBERADO_DO_ISOLAMENTO = 'alta_liberado_do_isolamento';
    const ALTA_ISOLAMENTO_SOCIAL = 'alta_isolamento_social';
    const EM_ACOMPANHAMENTO_ESTAVEL = 'em_acompanhamento_estavel';
    const EM_ACOMPANHAMENTO_GRAVE = 'em_acompanhamento_grave';
    const OBITO = 'obito';

    //pai
    const INCIDENCIA_DE_CONFIRMADOS = 'incidencia_de_confirmados';
    const MAPA_CALOR_INCIDENCIA_DE_CONFIRMADOS = 'mapa_calor_incedencia_de_confirmados';
    //pai
    const INCIDENCIA_DE_SUSPEITAS = 'incidencia_de_suspeitas';
    const MAPA_CALOR_INCIDENCIA_DE_SUSPEITAS = 'mapa_calor_incedencia_de_suspeitas';
}
