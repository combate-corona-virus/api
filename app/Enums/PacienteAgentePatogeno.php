<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PacienteAgentePatogeno extends Enum
{
    const COVID19 = 'COVID19';
    const INFLUENZA_H1N1 = 'Influenza H1N1';
    const INFLUENZA_H3 = 'Influenza H3';
    const INFLUENZA_B = 'Influenza B';
    const INFLUENZA_NAO_SUBTIPADA = 'Influenza não subtipada';
    const SRAG_NAO_ESPECIFICADO = 'SRAG não especificado';
    const SG_NAO_ESPECIFICADO = 'SG não especificado';
}
