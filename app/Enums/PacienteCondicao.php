<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PacienteCondicao extends Enum
{
    const ALTA_LIBERADO_DO_ISOLAMENTO = 'Alta - Liberado do isolamento';
    const ALTA_ISOLAMENTO_SOCIAL = 'Alta - Isolamento social';
    const EM_ACOMPANHAMENTO_ESTAVEL = 'Em acompanhamento - Estável';
    const EM_ACOMPANHAMENTO_GRAVE = 'Em acompanhamento - Grave';
    const OBITO = 'Óbito';
}
