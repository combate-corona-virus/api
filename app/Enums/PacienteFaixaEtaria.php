<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PacienteFaixaEtaria extends Enum
{
    const FAIXA_ETARIA_0_9 = '0 - 9';
    const FAIXA_ETARIA_10_19 = '10 - 19';
    const FAIXA_ETARIA_20_29 = '20 - 29';
    const FAIXA_ETARIA_30_39 = '30 - 39';
    const FAIXA_ETARIA_40_49 = '40 - 49';
    const FAIXA_ETARIA_50_59 = '50 - 59';
    const FAIXA_ETARIA_60_69 = '60 - 69';
    const FAIXA_ETARIA_70_79 = '70 - 79';
    const FAIXA_ETARIA_80_89 = '80 - 89';
    const FAIXA_ETARIA_90_MAIS = '90 - Mais';
}
