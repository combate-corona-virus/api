<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PacienteStatusExame extends Enum
{
    const COLETADO = 'Coletado';
    const CONCLUIDO = 'Concluído';
}
