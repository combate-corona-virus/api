<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PacienteTipo extends Enum
{
    const SRAG = 'SRAG';
    const SINDROME_GRIPAL = 'Síndrome gripal';
}
