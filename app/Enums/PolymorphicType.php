<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PolymorphicType extends Enum
{
    const PACIENTE = 'pacientes';
    const POLYMORPHIC_FIELD_NAME = 'tabela';
}
