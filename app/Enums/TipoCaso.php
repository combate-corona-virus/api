<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class TipoCaso extends Enum
{
    const SUSPEITOS = 'suspeitos';
    const CONFIRMADOS = 'confirmados';
    const OBITO = 'obito';
}
