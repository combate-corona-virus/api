<?php

namespace App\Error;

use GraphQL\Error\Error;
use GraphQL\Language\Source;

class ApplicationError extends Error
{
    const CODE_UNAUTHENTICATED = 'Unauthenticated.';

    private $catchMessages = [
        'Token não fornecido' => self::CODE_UNAUTHENTICATED,
    ];

    public function __construct(
        $message,
        $nodes = null,
        Source $source = null,
        $positions = null,
        $path = null,
        $previous = null,
        array $extensions = []
    ) {
        $errorProperties = $this->getErrorProperties(
            $message,
            $nodes,
            $source,
            $positions,
            $path,
            $previous,
            $extensions
        );
        parent::__construct(
            $errorProperties['message'],
            $errorProperties['nodes'],
            $errorProperties['source'],
            $errorProperties['positions'],
            $errorProperties['path'],
            $errorProperties['previous'],
            $errorProperties['extensions']
        );
    }

    private function getErrorProperties(
        $message,
        $nodes,
        Source $source,
        $positions,
        $path,
        $previous,
        $extensions
    ) {
        $errorProperties = null;
        foreach ($this->catchMessages as $errorCode => $catchMessage) {
            if ($message == $catchMessage) {
                $errorProperties = [
                    'message' => $message,
                    'nodes' => null,
                    'source' => null,
                    'positions' => $positions,
                    'path' => $path,
                    'previous' => null,
                    'extensions' => [
                        'extensions' => ['code' => $errorCode],
                    ],
                ];

                break;
            }
        }

        $originalErrorProperties = [
           'message' => $message,
           'nodes' => $nodes,
           'source' => $source,
           'positions' => $positions,
           'path' => $path,
           'previous' => $previous,
           'extensions' => $extensions,
        ];

        return (!empty($errorProperties)) ? $errorProperties : $originalErrorProperties;
    }
}
