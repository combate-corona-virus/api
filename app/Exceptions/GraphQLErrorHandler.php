<?php

namespace App\Exceptions;

use App\Error\ApplicationError;
use GraphQL\Error\Error;

class GraphQLErrorHandler implements \Nuwave\Lighthouse\Execution\ErrorHandler
{
    public static function handle(Error $error, \Closure $next): array
    {
        $error = new ApplicationError(
            $error->message,
            $error->getNodes(),
            $error->getSource(),
            $error->getPositions(),
            $error->getPath(),
            $error->getPrevious()
        );

        return $next($error);
    }
}
