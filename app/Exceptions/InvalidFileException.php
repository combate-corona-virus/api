<?php

namespace App\Exceptions;

class InvalidFileException extends \Exception
{
    public function __construct($message = '')
    {
        $message = "O arquivo enviado é inválido. \n" . $message;

        parent::__construct($message);
    }
}
