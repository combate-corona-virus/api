<?php

namespace App\GraphQL\Builders;

use App\Models\Chat;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Arr;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GetPaginatedGroupedChatBuilder
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args the arguments that were passed into the field
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context arbitrary data that is shared between all fields of a single query
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo information about the query itself, such as the execution state, the field name, path to the field from the root, and more
     *
     * @return mixed
     */
    public function handle($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $idMunicipio = $args['id_municipio'];
        $enviadoPor = $args['enviado_por'];

        $selectQuery = " WITH chat AS (
            select id, created_at, id_municipio,
                   ROW_NUMBER() OVER(PARTITION BY id_dispositivo
                                         ORDER BY created_at DESC) AS rk

            FROM chat where id_municipio = $idMunicipio AND enviado_por = '$enviadoPor')
        SELECT chat.id
            FROM chat
            WHERE chat.rk = 1 AND chat.id_municipio = $idMunicipio
            ORDER BY chat.created_at DESC";

        $ids = Arr::pluck(\DB::select($selectQuery), 'id');

        return Chat::whereIn('id', $ids);
    }
}
