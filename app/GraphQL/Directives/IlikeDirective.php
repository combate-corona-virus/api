<?php

namespace App\GraphQL\Directives;

use Nuwave\Lighthouse\Schema\Directives\WhereDirective as WhereDirective;

class IlikeDirective extends WhereDirective
{
    public function name(): string
    {
        return 'ilike';
    }

    /**
     * Add any "where ilike" clause to the builder that does not consider accents.
     *
     * @param  \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder  $builder
     * @param  mixed  $value
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function handleBuilder($builder, $value)
    {
        $columnName = $this->directiveArgValue('key', $this->definitionNode->name->value);

        $ilikeValue = str_replace("'", "%", $value);
        $ilikeValue = str_replace(" ", '%', $ilikeValue);
        $ilikeValue = "%$ilikeValue%";

        $builder->whereRaw("public.unaccent($columnName) ilike public.unaccent(:ilikeValue)", ['ilikeValue' => $ilikeValue])
            ->orderByRaw("$columnName <-> :value", ['value' => $value]);
        return $builder;
    }
}
