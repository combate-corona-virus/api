<?php

namespace App\GraphQL\Mutations;

use App\GraphQL\Queries\ServerInfo;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class ChangeTimezone
{
    private $serverInfo;

    public function __construct(ServerInfo $serverInfo)
    {
        $this->serverInfo = $serverInfo;
    }

    /**
     * Return a value for the field.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param array $args the arguments that were passed into the field
     * @param null|GraphQLContext $context arbitrary data that is shared between all fields of a single query
     * @param ResolveInfo $resolveInfo information about the query itself, such as the execution state, the field name, path to the field from the root, and more
     *
     * @return array
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        date_default_timezone_set($args['timezone']);

        return $this->serverInfo->resolve($rootValue, $args, $context, $resolveInfo);
    }
}
