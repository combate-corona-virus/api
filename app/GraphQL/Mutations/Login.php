<?php

namespace App\GraphQL\Mutations;

use Facades\App\Services\AuthService;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class Login
{
    /**
     * Login do Usuário.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param array $args the arguments that were passed into the field
     * @param null|GraphQLContext $context arbitrary data that is shared between all fields of a single query
     * @param ResolveInfo $resolveInfo information about the query itself, such as the execution state, the field name, path to the field from the root, and more
     *
     * @return mixed
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        return AuthService::login($args['email'], $args['password'], isset($args['remember']));
    }
}
