<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GetCalculoIprRegional
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $calculoIprRegional = \DB::select("SELECT t.*, situacao FROM referencia_situacao, 
        ( 
        WITH dados_ipr AS (
            SELECT
                boletim_regional.id_regiao_administrativa, current_timestamp AS data_calculo_boletim_regional,
                ROUND(quantidade_confirmados*100/quantidade_confirmados_previsto_fiocruz::NUMERIC,2) AS curva_epidemiologica,
                ROUND(quantidade_obitos*100/quantidade_confirmados::NUMERIC,2) AS obitos,
                quantidade_testagem AS testagem,
                quantidade_leitos_uti AS leitos, -- ** ATUALIZADO (VALOR ABSOLUTO) **
                ROUND(quantidade_leitos_uti_ocupados*100/quantidade_leitos_uti::NUMERIC,0) AS taxa_ocupacao, -- ** ATUALIZADO (ARREDONDA PARA INTEIRO) **
                indice_isolamento_social AS isolamento_social
                    FROM 
                        (
                            SELECT id_regiao_administrativa,
                            1210152::INT AS populacao, -- ** ATUALIZADO (VALOR USADO PELA PREFEITURA) **
                            SUM(quantidade_suspeitos) AS quantidade_suspeitos,
                            SUM(quantidade_confirmados) AS quantidade_confirmados,
                            SUM(quantidade_obitos) AS quantidade_obitos,
                            SUM(quantidade_descartados) AS quantidade_descartados,
                            SUM(quantidade_curados) AS quantidade_curados,
                            SUM(quantidade_obitos_suspeitos) AS quantidade_obitos_suspeitos,
                            SUM(quantidade_confirmados_previsto_fiocruz) AS quantidade_confirmados_previsto_fiocruz,
                            SUM(quantidade_testagem) AS quantidade_testagem,
                            SUM(quantidade_leitos_uti) AS quantidade_leitos_uti,
                            SUM(quantidade_leitos_uti_ocupados) AS quantidade_leitos_uti_ocupados,
                            AVG(indice_isolamento_social) AS indice_isolamento_social -- MÉDIA SIMPLES (DEFINIDO PELO MIGUEL EM 29/07).
                                FROM
                                (
                                    SELECT DISTINCT ON (id_municipio) id_regiao_administrativa, id_municipio, data, populacao,
                                    quantidade_suspeitos, quantidade_confirmados, quantidade_obitos, quantidade_descartados,
                                    quantidade_curados, quantidade_obitos_suspeitos, quantidade_confirmados_previsto_fiocruz, 
                                    quantidade_testagem, quantidade_leitos_uti, quantidade_leitos_uti_ocupados, indice_isolamento_social 
                                    FROM boletim_epidemiologico b JOIN municipio m ON b.id_municipio = m.id
                                    WHERE m.id_regiao_administrativa = (SELECT id FROM regiao_administrativa WHERE nome LIKE 'Região administrativa de Bauru') -- ** ALTERADO (= E LIKE) **
                                    AND b.data > current_timestamp - interval '48 hours'
                                    ORDER BY id_municipio, data DESC, b.created_at DESC -- ** ATUALIZADO PARA TRAZER O MAIS RECENTE **
                                ) AS t GROUP BY 1
                        ) AS boletim_regional
            ) 
        SELECT id_regiao_administrativa, data_calculo_boletim_regional,
        r1.valor_atribuido AS indice_curva_epidemiologica, r2.valor_atribuido AS indice_obitos, r3.valor_atribuido AS indice_testagem,
        r4.valor_atribuido AS indice_leitos, r5.valor_atribuido AS indice_taxa_ocupacao, r6.valor_atribuido AS indice_isolamento_social,
        ROUND(r1.valor_atribuido * (r2.valor_atribuido * 0.5 + r3.valor_atribuido * 0.5) * (r4.valor_atribuido * 0.3 + r5.valor_atribuido * 0.7) * r6.valor_atribuido,4) AS indice_total,
        ROUND(POWER (r1.valor_atribuido * (r2.valor_atribuido*0.5 + r3.valor_atribuido * 0.5) * (r4.valor_atribuido * 0.3 + r5.valor_atribuido * 0.7) * r6.valor_atribuido,0.25),4) AS ipr
        FROM referencia_curva_epidemiologica r1, referencia_obitos r2, 
        referencia_testagem r3, -- ** ATUALIZADO (VAI USAR A MESMA REFERÊNCIA DO MUNICIPAL) **
        referencia_leitos r4, -- ** ATUALIZADO (VAI USAR A MESMA REFERÊNCIA DO MUNICIPAL) **
        referencia_taxa_ocupacao r5, referencia_isolamento_social r6, dados_ipr 
        WHERE 
        dados_ipr.curva_epidemiologica BETWEEN r1.minimo AND r1.maximo
        AND dados_ipr.obitos BETWEEN r2.minimo AND r2.maximo
        AND dados_ipr.testagem BETWEEN r3.minimo AND r3.maximo
        AND dados_ipr.leitos BETWEEN r4.minimo AND r4.maximo
        AND dados_ipr.taxa_ocupacao BETWEEN r5.minimo AND r5.maximo
        AND dados_ipr.isolamento_social BETWEEN r6.minimo AND r6.maximo
        ) AS t
                WHERE t.ipr BETWEEN minimo AND maximo;");


        $data = [];
        foreach ($calculoIprRegional as $calculo) {
            array_push($data,
            [
                'name' => 'Data cálculos boletim regional',
                'value' => \Carbon\Carbon::parse($calculo->data_calculo_boletim_regional)->format('d/m/Y H:i'),
            ]);
            array_push($data,
            [
                'name' => 'Índice curva epidemiologia',
                'value' => $calculo->indice_curva_epidemiologica,
            ]);
            array_push($data,
            [
                'name' => 'Índice óbitos',
                'value' => $calculo->indice_obitos,
            ]);
            array_push($data,
            [
                'name' => 'Índice testagem',
                'value' => $calculo->indice_testagem,
            ]);
            array_push($data,
            [
                'name' => 'Índice leitos',
                'value' => $calculo->indice_leitos,
            ]);
            array_push($data,
            [
                'name' => 'Índice taxa ocupação',
                'value' => $calculo->indice_taxa_ocupacao,
            ]);
            array_push($data,
            [
                'name' => 'Índice isolamento social',
                'value' => $calculo->indice_isolamento_social,
            ]);
            array_push($data,
            [
                'name' => 'Índice total',
                'value' => $calculo->indice_total,
            ]);
            array_push($data,
            [
                'name' => 'IPR',
                'value' => $calculo->ipr,
            ]);
            array_push($data,
            [
                'name' => 'Situação',
                'value' => $calculo->situacao,
            ]);
        }

        return $data;
    }
}
