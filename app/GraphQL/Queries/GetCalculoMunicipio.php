<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GetCalculoMunicipio
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $calculoMunicipal = \DB::select("SELECT t.*, situacao FROM referencia_situacao, 
        ( 
        WITH dados_ipr AS (
            SELECT
                boletim_epidemiologico.id_municipio, data,
                ROUND(quantidade_confirmados*100/quantidade_confirmados_previsto_fiocruz::NUMERIC,2) AS curva_epidemiologica,
                ROUND(quantidade_obitos*100/quantidade_confirmados::NUMERIC,2) AS obitos,
                quantidade_testagem AS testagem,
                quantidade_leitos_uti AS leitos, -- ** ATUALIZADO (VALOR ABSOLUTO) **
                ROUND(quantidade_leitos_uti_ocupados*100/quantidade_leitos_uti::NUMERIC,0) AS taxa_ocupacao, -- ** ATUALIZADO (ARREDONDA PARA INTEIRO) **
                indice_isolamento_social AS isolamento_social
            FROM boletim_epidemiologico JOIN municipio ON boletim_epidemiologico.id_municipio = municipio.id
            WHERE data::DATE > current_timestamp - interval '48 hours' -- ATUAL (PASSANDO DATA). SE FOR ÚLTIMAS 48 HORAS: WHERE data > current_timestamp - interval '48 hours' 
            AND boletim_epidemiologico.id_municipio = (SELECT id FROM municipio WHERE id = {$args['id_municipio']}) AND quantidade_confirmados > 0 -- ** ALTERADO (=) **
            ORDER BY data DESC, boletim_epidemiologico.created_at DESC LIMIT 1 -- ** ADICIONADO PARA TRAZER O MAIS RECENTE **
                ) 
            SELECT id_municipio, data,
            r1.valor_atribuido AS indice_curva_epidemiologica, r2.valor_atribuido AS indice_obitos, r3.valor_atribuido AS indice_testagem,
            r4.valor_atribuido AS indice_leitos, r5.valor_atribuido AS indice_taxa_ocupacao, r6.valor_atribuido AS indice_isolamento_social,
            ROUND(r1.valor_atribuido * (r2.valor_atribuido * 0.5 + r3.valor_atribuido * 0.5) * (r4.valor_atribuido * 0.3 + r5.valor_atribuido * 0.7) * r6.valor_atribuido,4) AS indice_total,
            ROUND(POWER (r1.valor_atribuido * (r2.valor_atribuido*0.5 + r3.valor_atribuido * 0.5) * (r4.valor_atribuido * 0.3 + r5.valor_atribuido * 0.7) * r6.valor_atribuido,0.25),4) AS ipr
            FROM referencia_curva_epidemiologica r1, referencia_obitos r2, referencia_testagem r3,
            referencia_leitos r4, referencia_taxa_ocupacao r5, referencia_isolamento_social r6, dados_ipr 
            WHERE 
            dados_ipr.curva_epidemiologica BETWEEN r1.minimo AND r1.maximo
            AND dados_ipr.obitos BETWEEN r2.minimo AND r2.maximo
            AND dados_ipr.testagem BETWEEN r3.minimo AND r3.maximo
            AND dados_ipr.leitos BETWEEN r4.minimo AND r4.maximo
            AND dados_ipr.taxa_ocupacao BETWEEN r5.minimo AND r5.maximo
            AND dados_ipr.isolamento_social BETWEEN r6.minimo AND r6.maximo
            ) AS t
                WHERE t.ipr BETWEEN minimo AND maximo;");
	
        
        $data = [];
        foreach ($calculoMunicipal as $calculo) {
            array_push($data,
            [
                'name' => 'Data',
                'value' => \Carbon\Carbon::parse($calculo->data)->format('d/m/Y'),
            ]);
            array_push($data,
            [
                'name' => 'Índice curva epidemiologia',
                'value' => $calculo->indice_curva_epidemiologica,
            ]);
            array_push($data,
            [
                'name' => 'Índice óbitos',
                'value' => $calculo->indice_obitos,
            ]);
            array_push($data,
            [
                'name' => 'Índice testagem',
                'value' => $calculo->indice_testagem,
            ]);
            array_push($data,
            [
                'name' => 'Índice leitos',
                'value' => $calculo->indice_leitos,
            ]);
            array_push($data,
            [
                'name' => 'Índice taxa ocupação',
                'value' => $calculo->indice_taxa_ocupacao,
            ]);
            array_push($data,
            [
                'name' => 'Índice isolamento social',
                'value' => $calculo->indice_isolamento_social,
            ]);
            array_push($data,
            [
                'name' => 'Índice total',
                'value' => $calculo->indice_total,
            ]);
            array_push($data,
            [
                'name' => 'IPR',
                'value' => $calculo->ipr,
            ]);
            array_push($data,
            [
                'name' => 'Situação',
                'value' => $calculo->situacao,
            ]);
        }

        return $data;

    }
}
