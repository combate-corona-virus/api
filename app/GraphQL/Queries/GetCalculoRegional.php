<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GetCalculoRegional
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $calculoRegional = \DB::select("SELECT id_regiao_administrativa, quantidade_municipios, quantidade_preenchidos, 
        CASE 
            WHEN quantidade_municipios = quantidade_preenchidos THEN 'Dados preenchidos'
            ELSE 'Aguardando atualização'
        END AS mensagem
        FROM 
            (SELECT r.id AS id_regiao_administrativa, COUNT(m.id) AS quantidade_municipios, COALESCE(p.quantidade,0) AS quantidade_preenchidos
            FROM municipio m RIGHT JOIN regiao_administrativa r ON m.id_regiao_administrativa = r.id
            LEFT JOIN 
                (SELECT id_regiao_administrativa, COUNT(*) AS quantidade FROM 
                    (SELECT id_regiao_administrativa, t.*
                    FROM (SELECT id_municipio, MAX(data) AS data FROM boletim_epidemiologico GROUP BY 1) AS t 
                    JOIN municipio m ON t.id_municipio = m.id WHERE data > current_timestamp - interval '48 hours') r
                    GROUP BY 1)
                AS p ON p.id_regiao_administrativa = r.id
            GROUP BY 1, 3) AS t;");

        $data = [];
        foreach ($calculoRegional as $calculo) {
            array_push($data,
            [
                'name' => 'Quantidade município',
                'value' => $calculo->quantidade_municipios,
            ]);
            array_push($data,
            [
                'name' => 'Quantidade preenchidos',
                'value' => $calculo->quantidade_preenchidos,
            ]);
            array_push($data,
            [
                'name' => 'Situação',
                'value' => $calculo->mensagem,
            ]);
        }

        return $data;
    }
}
