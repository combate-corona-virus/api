<?php

namespace App\GraphQL\Queries;

use App\Models\Geocamada;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GetFeatureCollection
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args the arguments that were passed into the field
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context arbitrary data that is shared between all fields of a single query
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo information about the query itself, such as the execution state, the field name, path to the field from the root, and more
     *
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $geocamadas = Geocamada::whereIn('id', $args['id_geocamada'])->get();

        $data = [];
        $naoInformado = 'Não Informado';

        foreach ($geocamadas as $geocamada) {
            $query = $geocamada->pacientes()->bounded($args['bounds']);
            if (isset($args['id_municipio'])) {
                $query->where('id_municipio', $args['id_municipio']);
            }

            $pacientes = $query->get();

            $features = $pacientes->map(function ($paciente) use ($geocamada, $naoInformado) {
                return [
                    'type' => 'Feature',
                    'geometry' => $paciente->geom,
                    'properties' => [
                        'id' => $paciente->id,
                        'faixa_etaria' => $paciente->faixa_etaria->value ?? $naoInformado,
                        'sexo' => $paciente->sexo ?? $naoInformado,
                        'numero_notificacao' => $paciente->numero_notificacao ?? $naoInformado,
                        'condicao' => $paciente->condicao->value ?? $naoInformado,
                        'status_exame' => $paciente->status_exame->value ?? $naoInformado,
                        'nome_legivel' => $geocamada->nome_legivel,
                        'cor' => $geocamada->cor,
                        'icone_unicode' => $geocamada->icone_unicode,
                        'id_geocamada' => $geocamada->id,
                ],
            ];
            });

            $featureCollection = [
                'type' => 'FeatureCollection',
                'id_geocamada' => $geocamada->id,
                'geocamada' => $geocamada->nome_legivel,
                'features' => $features,
            ];

            array_push($data, $featureCollection);
        }

        return $data;
    }
}
