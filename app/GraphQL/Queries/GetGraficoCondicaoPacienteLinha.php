<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GetGraficoCondicaoPacienteLinha
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args the arguments that were passed into the field
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context arbitrary data that is shared between all fields of a single query
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo information about the query itself, such as the execution state, the field name, path to the field from the root, and more
     *
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $idMunicipio = $args['id_municipio'];

        $pacientes = collect(\DB::select("SELECT dia,
        SUM(t1) OVER (ORDER BY dia ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS alta_liberado_isolamento,
        SUM(t2) OVER (ORDER BY dia ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS alta_isolamento_social,
        SUM(t3) OVER (ORDER BY dia ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS em_acompanhamento_estavel,
        SUM(t4) OVER (ORDER BY dia ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS em_acompanhamento_grave,
        SUM(t5) OVER (ORDER BY dia ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS obito
          FROM (
          SELECT dia, COALESCE(t1.qtd,0) AS t1, COALESCE(t2.qtd,0) AS t2, COALESCE(t3.qtd,0) AS t3, COALESCE(t4.qtd,0) AS t4, COALESCE(t5.qtd,0) AS t5
          FROM (SELECT dia::date from generate_series('2020-02-01', current_date - INTERVAL '1 DAY', '1 day'::interval) dia) dia
              LEFT JOIN (SELECT data_notificacao, COUNT(data_notificacao) AS qtd FROM paciente WHERE deleted_at IS NULL AND condicao = 'Alta - Liberado do isolamento' AND id_municipio = $idMunicipio GROUP BY 1) t1 ON t1.data_notificacao = dia
              LEFT JOIN (SELECT data_notificacao, COUNT(data_notificacao) AS qtd FROM paciente WHERE deleted_at IS NULL AND condicao = 'Alta - Isolamento social' AND id_municipio = $idMunicipio GROUP BY 1) t2 ON t2.data_notificacao = dia
              LEFT JOIN (SELECT data_notificacao, COUNT(data_notificacao) AS qtd FROM paciente WHERE deleted_at IS NULL AND condicao = 'Em acompanhamento - Estável' AND id_municipio = $idMunicipio GROUP BY 1) t3 ON t3.data_notificacao = dia
              LEFT JOIN (SELECT data_notificacao, COUNT(data_notificacao) AS qtd FROM paciente WHERE deleted_at IS NULL AND condicao = 'Em acompanhamento - Grave' AND id_municipio = $idMunicipio GROUP BY 1) t4 ON t4.data_notificacao = dia
              LEFT JOIN (SELECT data_notificacao, COUNT(data_notificacao) AS qtd FROM paciente WHERE deleted_at IS NULL AND condicao = 'Óbito' AND id_municipio = $idMunicipio GROUP BY 1) t5 ON t5.data_notificacao = dia
          ) AS dados ORDER BY dia;"));

        $altaLiberadoIsolamentoSeries = $pacientes->map(function ($item) {
            return [
                'name' => \Carbon\Carbon::parse($item->dia)->format('d/m'),
                'value' => $item->alta_liberado_isolamento,
            ];
        });

        $altaIsolamentoSocialSeries = $pacientes->map(function ($item) {
            return [
                'name' => \Carbon\Carbon::parse($item->dia)->format('d/m'),
                'value' => $item->alta_isolamento_social,
            ];
        });

        $emAcompanhamentoEstavelSeries = $pacientes->map(function ($item) {
            return [
                'name' => \Carbon\Carbon::parse($item->dia)->format('d/m'),
                'value' => $item->em_acompanhamento_estavel,
            ];
        });

        $emAcompanhamentoGraveSeries = $pacientes->map(function ($item) {
            return [
                'name' => \Carbon\Carbon::parse($item->dia)->format('d/m'),
                'value' => $item->em_acompanhamento_grave,
            ];
        });

        $obitSeries = $pacientes->map(function ($item) {
            return [
                'name' => \Carbon\Carbon::parse($item->dia)->format('d/m'),
                'value' => $item->obito,
            ];
        });

        return [
            [
                'name' => 'Alta - Liberado do isolamento',
                'series' => $altaLiberadoIsolamentoSeries,
            ],

            [
                'name' => 'Alta - Isolamento social',
                'series' => $altaIsolamentoSocialSeries,
            ],

            [
                'name' => 'Em acompanhamento - Estável',
                'series' => $emAcompanhamentoEstavelSeries,
            ],

            [
                'name' => 'Em acompanhamento - Grave',
                'series' => $emAcompanhamentoGraveSeries,
            ],

            [
                'name' => 'Óbito',
                'series' => $obitSeries,
            ],
        ];
    }
}
