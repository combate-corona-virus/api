<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GetGraficoCondicaoPacientePizza
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args the arguments that were passed into the field
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context arbitrary data that is shared between all fields of a single query
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo information about the query itself, such as the execution state, the field name, path to the field from the root, and more
     *
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $pacientes = \DB::select("SELECT paciente_condicao.type AS condicao , COUNT(condicao)
        FROM (SELECT unnest(enum_range(NULL::paciente_condicao))) AS paciente_condicao(type)
        FULL JOIN paciente ON paciente.condicao=paciente_condicao.type
        AND id_municipio = {$args['id_municipio']} WHERE paciente.deleted_at IS NULL AND paciente_condicao.type IS NOT NULL GROUP BY 1 ORDER BY 1;");

        $data = [];
        foreach ($pacientes as $paciente) {
            array_push($data, [
                'name' => $paciente->condicao,
                'value' => $paciente->count,
            ]);
        }

        return $data;
    }
}
