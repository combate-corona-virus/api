<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GetGraficoFaixaEtaria
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args the arguments that were passed into the field
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context arbitrary data that is shared between all fields of a single query
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo information about the query itself, such as the execution state, the field name, path to the field from the root, and more
     *
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $pacientes = \DB::select('SELECT faixa_etaria.type AS faixa, COALESCE(t.qtd,0) as qtd
        FROM (SELECT unnest(enum_range(NULL::paciente_faixa_etaria))) AS faixa_etaria(type)
        FULL JOIN (SELECT faixa_etaria, COUNT(faixa_etaria) AS qtd
        FROM paciente WHERE deleted_at IS NULL AND agente_patogeno = \'COVID19\' AND condicao = \'Óbito\'
        AND id_municipio = ' . $args['id_municipio'] . ' GROUP BY 1) t
        ON t.faixa_etaria = faixa_etaria.type GROUP BY 1, 2 ORDER BY 1;');

        $data = [];
        foreach ($pacientes as $paciente) {
            array_push($data, [
                'name' => $paciente->faixa,
                'value' => $paciente->qtd,
            ]);
        }

        return $data;
    }
}
