<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GetGraficoNorificacoesPorDia
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args the arguments that were passed into the field
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context arbitrary data that is shared between all fields of a single query
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo information about the query itself, such as the execution state, the field name, path to the field from the root, and more
     *
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $idMunicipio = $args['id_municipio'];

        $pacientes = collect(
            \DB::select("SELECT dia,
            SUM(t1) OVER (ORDER BY dia ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS srag,
            SUM(t2) OVER (ORDER BY dia ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS sindrome_gripal
            FROM (
                SELECT dia, COALESCE(t1.qtd,0) AS t1, COALESCE(t2.qtd,0) AS t2
                FROM (SELECT dia::date from generate_series('2020-02-01', current_date - INTERVAL '1 DAY', '1 day'::interval) dia) dia
                LEFT JOIN (SELECT data_notificacao, COUNT(data_notificacao) AS qtd FROM paciente WHERE deleted_at IS NULL AND tipo = 'SRAG' AND id_municipio = $idMunicipio GROUP BY 1) t1 ON t1.data_notificacao = dia
                LEFT JOIN (SELECT data_notificacao, COUNT(data_notificacao) AS qtd FROM paciente WHERE deleted_at IS NULL AND tipo = 'Síndrome gripal' AND id_municipio = $idMunicipio GROUP BY 1) AS t2 ON t2.data_notificacao = dia
            ) AS dados ORDER BY dia;")
        );

        return $pacientes->map(function ($paciente) {
            return [
                'name' => \Carbon\Carbon::parse($paciente->dia)->format('d/m'),
                'series' => [
                    [
                        'name' => 'SRAG',
                        'value' => $paciente->srag,
                    ],
                    [
                        'name' => 'Síndrome gripal',
                        'value' => $paciente->sindrome_gripal,
                    ],
                ],
            ];
        });
    }
}
