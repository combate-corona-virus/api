<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GetGraficoTotalizadores
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args the arguments that were passed into the field
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context arbitrary data that is shared between all fields of a single query
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo information about the query itself, such as the execution state, the field name, path to the field from the root, and more
     *
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $pacientes = \DB::select('SELECT
        COALESCE(t1.qtd,0) AS "SUSPEITO",
        COALESCE(t2.qtd,0) AS "CONFIRMADO",
        COALESCE(t3.qtd,0) AS "OBITO",
        COALESCE(t4.qtd,0) AS "DESCARTADO",
        COALESCE(t5.qtd,0) AS "OBITO_SUSPEITO",
        COALESCE(t6.qtd,0) AS "CURADO"
        FROM (SELECT COUNT(*) AS qtd
        FROM paciente WHERE deleted_at IS NULL AND status_exame = \'Coletado\'
        AND id_municipio = ' . $args['id_municipio'] . ') t1, (SELECT COUNT(*) AS qtd
        FROM paciente WHERE deleted_at IS NULL AND status_exame = \'Concluído\'
        AND agente_patogeno = \'COVID19\'
        AND id_municipio = ' . $args['id_municipio'] . ') t2, (SELECT COUNT(*) AS qtd
        FROM paciente WHERE deleted_at IS NULL AND status_exame = \'Concluído\'
        AND agente_patogeno = \'COVID19\' AND condicao = \'Óbito\'
        AND id_municipio = ' . $args['id_municipio'] . ') t3, (SELECT COUNT(*) AS qtd
        FROM paciente WHERE deleted_at IS NULL AND status_exame = \'Concluído\'
        AND (agente_patogeno != \'COVID19\' OR agente_patogeno IS NULL)
        AND id_municipio = ' . $args['id_municipio'] . ') t4, (SELECT COUNT(*) AS qtd
        FROM paciente WHERE deleted_at IS NULL AND status_exame = \'Coletado\'
        AND condicao = \'Óbito\'
        AND id_municipio = ' . $args['id_municipio'] . ') t5, (SELECT COUNT(*) AS qtd
        FROM paciente WHERE deleted_at IS NULL AND status_exame = \'Concluído\'
        AND (agente_patogeno = \'COVID19\')
        AND condicao = \'Alta - Liberado do isolamento\'
        AND id_municipio = ' . $args['id_municipio'] . ') t6;');

        $data = [];
        foreach ($pacientes as $paciente) {
            array_push($data,
            [
                'name' => 'Casos <br>confirmados',
                'value' => $paciente->CONFIRMADO,
            ]);
            array_push($data,
            [
                'name' => 'Casos <br>descartados',
                'value' => $paciente->DESCARTADO,
            ]);
            array_push($data,
            [
                'name' => 'Aguardando <br>resultado',
                'value' => $paciente->SUSPEITO,
            ]);
            array_push($data,
            [
                'name' => 'Casos <br>curados',
                'value' => $paciente->CURADO,
            ]);
            array_push($data,
            [
                'name' => 'Óbitos <br>suspeitos',
                'value' => $paciente->OBITO_SUSPEITO,
            ]);
            array_push($data,
            [
                'name' => 'Óbitos <br>confirmados',
                'value' => $paciente->OBITO,
            ]);
        }

        return $data;
    }
}
