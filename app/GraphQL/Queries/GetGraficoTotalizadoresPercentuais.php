<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GetGraficoTotalizadoresPercentuais
{
    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args the arguments that were passed into the field
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context arbitrary data that is shared between all fields of a single query
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo information about the query itself, such as the execution state, the field name, path to the field from the root, and more
     *
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $incidencia = \DB::select('SELECT ROUND((t.qtd/m.populacao::NUMERIC)*100000,2) AS "INCIDENCIA"
        FROM (SELECT id_municipio, COUNT(*) AS qtd
        FROM paciente WHERE deleted_at IS NULL AND status_exame = \'Concluído\'
        AND agente_patogeno = \'COVID19\' GROUP BY 1) t
        JOIN municipio m ON t.id_municipio = m.id WHERE m.id = ' . $args['id_municipio'] . ';');

        $mortalidade = \DB::select('SELECT ROUND((t.qtd/m.populacao::NUMERIC)*100000,2) AS "MORTALIDADE"
        FROM (SELECT id_municipio, COUNT(*) AS qtd
        FROM paciente WHERE deleted_at IS NULL AND status_exame = \'Concluído\'
        AND agente_patogeno = \'COVID19\' AND condicao = \'Óbito\' GROUP BY 1) t
        JOIN municipio m ON t.id_municipio = m.id WHERE m.id = ' . $args['id_municipio'] . ';');

        $letalidade = \DB::select('SELECT ROUND((t1.qtd)::NUMERIC/(t2.qtd)::NUMERIC*100,2) AS "LETALIDADE"
        FROM municipio m JOIN (SELECT id_municipio, COUNT(*) AS qtd
        FROM paciente WHERE deleted_at IS NULL AND status_exame = \'Concluído\'
        AND agente_patogeno = \'COVID19\' AND condicao = \'Óbito\' GROUP BY 1) t1
        ON t1.id_municipio = m.id JOIN (SELECT id_municipio, COUNT(*) AS qtd
        FROM paciente WHERE deleted_at IS NULL AND status_exame = \'Concluído\' AND agente_patogeno = \'COVID19\'
        GROUP BY 1) t2 ON t2.id_municipio = m.id WHERE m.id = ' . $args['id_municipio'] . ';');

        $data = [];
        array_push($data, [
            'name' => 'Incidência',
            'formated_name' => '<b>Incidência</b> <br>(casos por cem mil hab.)',
            'value' => $incidencia[0]->INCIDENCIA ?? 0,
            'extra' => [
                'is_por_mil' => true,
            ],
        ]);

        array_push($data, [
            'name' => 'Mortalidade',
            'formated_name' => '<b>Mortalidade</b> <br>(óbitos por cem mil hab.)',
            'value' => $mortalidade[0]->MORTALIDADE ?? 0,
            'extra' => [
                'is_por_mil' => true,
            ],
        ]);

        array_push($data, [
            'name' => 'Letalidade',
            'formated_name' => '<b>Letalidade</b> <br>(óbitos pelo total)',
            'value' => $letalidade[0]->LETALIDADE ?? 0,
            'extra' => [
                'is_por_mil' => false,
            ],

        ]);

        return $data;
    }
}
