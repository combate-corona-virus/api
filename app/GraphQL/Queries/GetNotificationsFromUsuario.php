<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GetNotificationsFromUsuario
{
    /**
     * Return a value for the field.
     *
     * @param  null  $usuario
     * @param  mixed[]  $args the arguments that were passed into the field
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context arbitrary data that is shared between all fields of a single query
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo information about the query itself, such as the execution state, the field name, path to the field from the root, and more
     *
     * @return mixed
     */
    public function resolve($usuario, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $fields = implode(',', array_keys($resolveInfo->getFieldSelection()));
        $notifications = $usuario->notifications()->selectRaw($fields)->get();

        return $notifications;
    }
}
