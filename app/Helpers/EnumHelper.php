<?php

namespace App\Helpers;

use BenSampo\Enum\Enum;
use DB as DB;

class EnumHelper
{
    public function exists(String $enumName)
    {
        $result = DB::select("SELECT 1 as exists FROM pg_type WHERE typname = '$enumName'");

        return isset($result[0]) && (bool) $result[0]->exists;
    }

    public function create(String $enumName, array $enumValues)
    {
        $values = "'" . implode("', '", $enumValues) . "'";

        if (!$this->exists($enumName)) {
            DB::statement("CREATE TYPE $enumName AS ENUM ($values);");
        }
    }

    public function rename(String $currentName, String $newName)
    {
        if ($this->exists($currentName) && !$this->exists($newName)) {
            DB::statement("ALTER TYPE $currentName RENAME TO $newName;");
        }
    }

    public function drop(String $enumName)
    {
        if ($this->exists($enumName)) {
            DB::statement("DROP TYPE $enumName");
        }
    }

    public function replaceValues(String $enumName, array $enumValues, String $newName = '')
    {
        if ($this->exists($enumName)) {
            // descobre quais tabelas estão usando o enum atual
            $tablesColumnsUsingEnum = DB::select("SELECT c.table_name, c.column_name from INFORMATION_SCHEMA.columns c
            INNER JOIN INFORMATION_SCHEMA.tables t ON t.table_name = c.table_name
            WHERE c.data_type = 'USER-DEFINED' AND udt_name = '$enumName'");

            // renomeia o enum atual pra _old
            // isso faz com que as tabelas que estejam usando o enum atuam, usem o _old também.
            $tmpName = $enumName . '_old';
            $this->rename($enumName, $tmpName);

            $enumName = empty($newName) ? $enumName : $newName;
            $this->create($enumName, $enumValues);

            // altera as tabelas para usarem o novo enum
            for ($i = 0; $i < count($tablesColumnsUsingEnum); ++$i) {
                $table = $tablesColumnsUsingEnum[$i]->table_name;
                $column = $tablesColumnsUsingEnum[$i]->column_name;
                DB::statement("ALTER TABLE $table ALTER COLUMN $column TYPE $enumName USING $column::text::$enumName;");
            }

            $this->drop($tmpName);
        }
    }

    public function addValue(String $enumName, String $newValue)
    {
        if ($this->exists($enumName)) {
            DB::statement("ALTER TYPE $enumName ADD VALUE IF NOT EXISTS '$newValue';");
        }
    }
}
