<?php

namespace App\Helpers;

class FeaturesHelper
{
    private $isEnableImportBoletimEpidemiologico;

    public function __construct()
    {
        $this->isEnableImportBoletimEpidemiologico = config('features.enable_import_boletim_epidemiologico');
    }

    public function canImportBoletimEpidemiologico()
    {
        return $this->isEnableImportBoletimEpidemiologico;
    }
}
