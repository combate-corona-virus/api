<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Response;
use Facades\App\Services\StorageService;
use Illuminate\Http\Request;

class ArquivoController extends Controller
{
    public function uploadImagem(Request $request)
    {
        $files = $request->files->all();

        StorageService::storeImagens($files);

        return Response::send(true);
    }

    public function upload(Request $request)
    {
        $files = $request->files->all();

        $response = StorageService::storeArquivos($files);

        return Response::send($response);
    }

    public function download(int $id)
    {
        return StorageService::downloadArquivoById($id);
    }

    public function delete(int $id)
    {
        $response = StorageService::deleteByIdArquivo($id);

        return Response::send($response);
    }
}
