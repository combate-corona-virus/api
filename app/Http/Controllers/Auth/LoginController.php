<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Response;
use Facades\App\Services\AuthService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Login do usuário.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function login(Request $request)
    {
        $loggedUser = AuthService::login($request->email, $request->password, isset($request->remember));

        return Response::send($loggedUser);
    }

    /**
     * Login do usuário.
     *
     * @return mixed
     */
    public function logout()
    {
        return  Response::send(AuthService::logout());
    }
}
