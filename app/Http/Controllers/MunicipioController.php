<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Response;
use Facades\App\Services\StorageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MunicipioController extends Controller
{
    /**
     * Habilita o middleware auth:api para requisições do Convênio.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function uploadLogo(Request $request)
    {
        $logo = collect($request->files)->first();
        $idMunicipio = Auth::user()->id_municipio;

        $result = StorageService::storeLogoMunicipio($idMunicipio, $logo);

        return Response::send($result);
    }
}
