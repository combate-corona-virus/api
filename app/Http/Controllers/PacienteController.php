<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Response;
use App\Imports\ImportPaciente;
use Facades\App\Services\StorageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class PacienteController extends Controller
{
    public function importPaciente(Request $request)
    {
        Excel::import(new ImportPaciente(Auth::user()), $request->file('arquivo'));

        return Response::send(true);
    }

    public function downloadTemplate()
    {
        return StorageService::downloadFromLocalStorage(
            'importacao/templates/Paciente-COVID19-Exemplo.xlsx',
            'Paciente-COVID19-Exemplo.xlsx'
        );
    }
}
