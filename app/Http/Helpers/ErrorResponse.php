<?php

namespace App\Http\Helpers;

class ErrorResponse
{
    /**
     * Envia resposta de erro.
     *
     * @param \Exception $exception
     */
    public static function send(\Exception $exception)
    {
        $exceptionDictionary =
            [
                'Illuminate\Http\Exception\PostTooLargeException' => [
                    'code' => 413,
                    'message' => 'Erro no servidor. O tamanho máximo da requisição é de: ' . ini_get('post_max_size'),
                ],
                'Symfony\Component\HttpKernel\Exception\NotFoundHttpException' => [
                    'code' => 404,
                    'message' => null,
                ],
                'Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException' => [
                    'code' => 405,
                    'message' => null,
                ],
                'Illuminate\Database\QueryException' => [
                    'code' => 422,
                    'message' => null,
                ],
                'Illuminate\Database\Eloquent\ModelNotFoundException' => [
                    'code' => 404,
                    'message' => null,
                ],
                'Illuminate\Auth\AuthenticationException' => [
                    'code' => 401,
                    'message' => 'Usuário não autenticado!',
                ],
                'InvalidArgumentException' => [
                    'code' => 400,
                    'message' => 'Preencha os campos requeridos.',
                ],
            ];

        $key = get_class($exception);

        $responseObject = new ResponseObject(null, ResponseObject::ERROR_STATUS_CODE);
        $responseObject->setFailureStatus();
        $responseObject->setTrace($exception);
        $responseObject->setError($exception);

        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\HttpException) {
            $responseObject->setCode($exception->getStatusCode());
            $responseObject->setErrorMessage($exception->getMessage());
        } elseif (isset($exceptionDictionary[$key])) {
            $exceptionStatusCodeMessage = $exceptionDictionary[$key];

            $responseObject->setErrorMessage($exceptionStatusCodeMessage['message']);
            $responseObject->setCode($exceptionStatusCodeMessage['code']);
        }

        return $responseObject->send();
    }
}
