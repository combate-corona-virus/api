<?php

namespace App\Http\Helpers;

class ListResponse extends Response
{
    /**
     * Envia resposta para o client.
     *
     * @param null|mixed $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function send($data)
    {
        $data = [$data];
        $response = new ResponseObject($data);

        return $response->send();
    }
}
