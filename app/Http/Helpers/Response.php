<?php

namespace App\Http\Helpers;

class Response
{
    /**
     * Envia resposta para o client.
     *
     * @param null|mixed $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function send($data)
    {
        $response = new ResponseObject($data);

        return $response->send();
    }
}
