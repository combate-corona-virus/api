<?php

namespace App\Http\Helpers;

class ResponseObject
{
    private $code;

    private $data;

    private $status;

    private $error;

    private $message;

    private $trace;

    public const DEFAULT_STATUS = 'success';
    public const DEFAULT_STATUS_CODE = 200;
    public const DEFAULT_ERROR_MESSAGE = 'Erro no servidor';
    public const ERROR_STATUS_CODE = 500;

    /**
     * Classe que define o padrão de resposta.
     *
     * @param null|mixed $data
     * @param null|int $code
     * @param null|string $status
     * @param null|string $message
     */
    public function __construct($data, int $code = self::DEFAULT_STATUS_CODE, string $status = self::DEFAULT_STATUS, string $message = null)
    {
        $this->data = $data ?? null;
        $this->code = $code;
        $this->status = $status;
        $this->message = $message;
    }

    /**
     * Envia resposta no formato json.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function send()
    {
        $response = [
            'code' => $this->code,
            'status' => $this->status,
            'data' => $this->data,
            'message' => $this->message,
            'error' => $this->error,
            'trace' => $this->trace,
        ];

        return response()->json($response, $this->code);
    }

    /**
     * Seta mensagem de erro.
     *
     * @param null|string $message
     */
    public function setErrorMessage(string $message = null)
    {
        $this->message = $message ?? self::DEFAULT_ERROR_MESSAGE;

        return $this;
    }

    /**
     * Seta código de erro.
     *
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code ?? self::DEFAULT_STATUS_CODE;

        return $this;
    }

    /**
     * Seta o nome do erro.
     *
     * @param  \Exception $exception
     */
    public function setError(\Exception $exception)
    {
        $message = $exception->getMessage();

        $this->error = !empty($message) ? $message : get_class($exception);

        return $this;
    }

    /**
     * Seta a resposta como falha.
     */
    public function setFailureStatus()
    {
        $this->status = 'failure';

        return $this;
    }

    /**
     * Seta o trace.
     *
     * @param \Exception $exception
     */
    public function setTrace(\Exception $exception)
    {
        $this->trace = in_array(config('app.env'), ['local', 'develop', 'testing', 'stage']) ? $exception->__toString() : null;

        return $this;
    }
}
