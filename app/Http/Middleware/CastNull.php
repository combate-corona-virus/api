<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TransformsRequest;

class CastNull extends TransformsRequest
{
    /**
     * The attributes that should not be transformed.
     *
     * @var array
     */
    protected $except = [

    ];

    /**
     * Transform the given value.
     *
     * @param  string  $key
     * @param  mixed  $value
     *
     * @return mixed
     */
    protected function transform($key, $value)
    {
        if (in_array($key, $this->except, true) || !is_string($value)) {
            return $value;
        }

        if (strcasecmp(trim($value), 'null') == 0) {
            return null;
        }

        return $value;
    }
}
