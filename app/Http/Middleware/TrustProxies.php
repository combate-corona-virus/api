<?php

namespace App\Http\Middleware;

use Fideloper\Proxy\TrustProxies as Middleware;
use Illuminate\Http\Request;

class TrustProxies extends Middleware
{
    /**
     * The trusted proxies for the application.
     *
     * @var null|array|string
     */
    protected $proxies = '*';

    /**
     * The proxy header mappings.
     *
     * @var null|int|string
     */
    protected $headers = Request::HEADER_X_FORWARDED_ALL;
}
