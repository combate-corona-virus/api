<?php

namespace App\Imports;

use App\Models\BoletimEpidemiologico;
use App\Models\Municipio;
use Illuminate\Support\Facades\Log as Log;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMapping;

class ImportBoletimEpidemiologico implements ToModel, WithChunkReading, WithHeadingRow, WithMapping //, ShouldQueue
{
    /**
     * @param array $row
     *
     * @return null|\Illuminate\Database\Eloquent\Model
     */
    public function model(array $dados)
    {
        if (!$dados['id_municipio']) {
            return null;
        }

        try {
            $boletimEpidemiologico = BoletimEpidemiologico::withTrashed()
                ->where('id_municipio', $dados['id_municipio'])
                ->where('data', $dados['data'])
                ->first() ?? new BoletimEpidemiologico();

            if ($boletimEpidemiologico->id_usuario) {
                return $boletimEpidemiologico; //não faz nada pois foi alimentado via sistema.
            }

            $boletimEpidemiologico->fill($dados);

            return $boletimEpidemiologico;
        } catch (\Exception $exception) {
            Log::error("Erro na linha {$this->rowCount} ================\n" . json_encode($dados) . '\n' . $exception->getMessage());

            throw $exception;
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function map($row): array
    {
        return $this->mapRowToDBColumns($row);
    }

    private function mapRowToDBColumns($row)
    {
        $rowsMap = $this->rowsToDBColumnsMapping();

        $data = $rowsMap->mapWithKeys(function ($item, $key) use ($row) {
            return [$item => $row[$key]];
        })->toArray();

        $data['id_municipio'] = Municipio::where('geocodigo', $data['geocodigo'])->select('id')->first()->id ?? null;
        $data['quantidade_suspeitos'] = 0;
        $data['quantidade_obitos'] = $data['quantidade_obitos'] ?? 0;
        $data['quantidade_confirmados'] = $data['quantidade_confirmados'] ?? 0;

        unset($data['geocodigo']);

        return $data;
    }

    private function rowsToDBColumnsMapping()
    {
        return
            collect([
                'date' => 'data',
                'state' => 'uf',
                'city_ibge_code' => 'geocodigo',
                'confirmed' => 'quantidade_confirmados',
                'deaths' => 'quantidade_obitos',
            ]);
    }
}
