<?php

namespace App\Imports;

use App\Models\Municipio;
use App\Models\UnidadeFederativa;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use MStaack\LaravelPostgis\Geometries\Point;

class ImportMunicipio implements ToModel, WithChunkReading, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return null|\Illuminate\Database\Eloquent\Model
     */
    public function model(array $row)
    {
        $uf = UnidadeFederativa::where('nome', Str::title($row['uf']))->firstOrFail();

        $geomCentro = new Point($row['latitude'], $row['longitude']);

        $dados = [
            'id_unidade_federativa' => $uf->id,
            'nome' => Str::title($row['municipio']),
            'geocodigo' => $row['geocodigo'],
            'populacao' => $row['populacao'],
            'geom_centro' => $geomCentro,
        ];

        return new Municipio($dados);
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
