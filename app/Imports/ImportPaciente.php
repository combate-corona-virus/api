<?php

namespace App\Imports;

use App\Enums\PacienteAgentePatogeno;
use App\Enums\PacienteCondicao;
use App\Enums\PacienteFaixaEtaria;
use App\Enums\PacienteStatusExame;
use App\Enums\PacienteTipo;
use App\Models\Municipio;
use App\Models\Paciente;
use App\Services\LocationService;
use Illuminate\Support\Facades\Log as Log;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMapping;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Events\AfterImport;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class ImportPaciente implements ToModel, WithChunkReading, WithHeadingRow, WithMultipleSheets, WithMapping, WithEvents //, ShouldQueue
{
    use RegistersEventListeners;

    private $sinamArray = [];

    private $usuario;

    private $rowCount;

    private $locationService;

    public function __construct($usuario)
    {
        $this->usuario = $usuario;
        $this->locationService = \App::make(LocationService::class);
    }

    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }

    /**
     * @param array $row
     *
     * @return null|\Illuminate\Database\Eloquent\Model
     */
    public function model(array $data)
    {
        ++$this->rowCount;

        try {
            array_push($this->sinamArray, $data['numero_notificacao']);
            $paciente = Paciente::withTrashed()->where('numero_notificacao', $data['numero_notificacao'])->first() ?? new Paciente();
            $paciente->fill($data);

            if ($paciente->endereco && ($paciente->isDirty('endereco') || !$paciente->geom)) {
                $paciente->geom = $this->locationService->getPointFromEndereco($paciente->endereco);
                $paciente->setGeomCentroMunicipioIfMissing();
            }

            $paciente->save();

            return $paciente;
        } catch (\Exception $exception) {
            Log::error("Erro na linha {$this->rowCount} ================\n" . json_encode($data) . '\n' . $exception->getMessage());

            throw $exception;
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function map($row): array
    {
        return $this->mapRowToDBColumns($row);
    }

    private function mapRowToDBColumns($row)
    {
        $rowsMap = $this->rowsToDBColumnsMapCollection();

        $mappedCollection = $rowsMap->mapWithKeys(function ($item, $key) use ($row) {
            return [$item => $row[$key]];
        });

        $idMunicipio = $this->usuario->id_municipio ?? Municipio::isBauru()->first()->id;

        $data = $mappedCollection->toArray();
        $data['id_municipio'] = $idMunicipio;
        $data['deleted_at'] = null;

        $data['agente_patogeno'] = $this->mapEnumValues(PacienteAgentePatogeno::getValues(), $data['agente_patogeno']);
        $data['condicao'] = $this->mapEnumValues(PacienteCondicao::getValues(), $data['condicao']);
        $data['status_exame'] = $this->mapEnumValues(PacienteStatusExame::getValues(), $data['status_exame']);
        $data['tipo'] = $this->mapEnumValues(PacienteTipo::getValues(), $data['tipo']);
        $data['faixa_etaria'] = $this->mapFaixaEtaria($data['faixa_etaria']);

        $data['data_notificacao'] = isset($data['data_notificacao']) ?
            \Carbon\Carbon::createFromTimestamp(Date::excelToTimestamp($data['data_notificacao'])) : null;
        $data['data_alta_obito'] = isset($data['data_alta_obito']) ?
            \Carbon\Carbon::createFromTimestamp(Date::excelToTimestamp($data['data_alta_obito'])) : null;
        $data['data_coleta'] = isset($data['data_coleta']) ?
            \Carbon\Carbon::createFromTimestamp(Date::excelToTimestamp($data['data_coleta'])) : null;

        return $data;
    }

    private function mapEnumValues(array $enumValues, $value)
    {
        if (!$value || empty($value)) {
            return null;
        }

        $foundValue = null;
        foreach ($enumValues as $enumValue) {
            if (Str::upper($enumValue) == Str::upper(trim($value))) {
                $foundValue = $enumValue;
            }
        }

        if (!$foundValue) {
            throw new \Exception("Enum {$value} inválido");
        }

        return $foundValue;
    }

    private function mapFaixaEtaria($value)
    {
        if (!$value || empty($value)) {
            return null;
        }

        $formatedValue = str_replace(' ', '', $value);
        $splitedVal = explode('-', $formatedValue);
        $formatedValue = implode(' - ', $splitedVal);

        return $this->mapEnumValues(PacienteFaixaEtaria::getValues(), $value);
    }

    private function rowsToDBColumnsMapCollection()
    {
        return
            collect([
                'tipo' => 'tipo',
                'num_de_notificacao_sinan' => 'numero_notificacao',
                'endereco_completo' => 'endereco',
                'residencia' => 'residencia',
                'data_da_notificacao' => 'data_notificacao',
                'sexo' => 'sexo',
                'idade' => 'idade',
                'faixa_etaria' => 'faixa_etaria',
                'condicao_do_paciente' => 'condicao',
                'data_do_obito_ou_alta' => 'data_alta_obito',
                'status_do_exame' => 'status_exame',
                'data_da_coleta' => 'data_coleta',
                'agente_patogeno' => 'agente_patogeno',
            ]);
    }

    public static function afterImport(AfterImport $event)
    {
        // Paciente::whereNotIn('numero_notificacao', $event->getConcernable()->sinamArray)->delete();
    }
}
