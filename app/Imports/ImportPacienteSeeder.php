<?php

namespace App\Imports;

use App\Models\Municipio;
use App\Models\Paciente;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use MStaack\LaravelPostgis\Geometries\Point;

class ImportPacienteSeeder implements ToModel, WithChunkReading, WithHeadingRow //, ShouldQueue
{
    /**
     * @param array $row
     *
     * @return null|\Illuminate\Database\Eloquent\Model
     */
    public function model(array $row)
    {
        $bauru = Municipio::isBauru()->first();

        $point = new Point($row['latitude'], $row['longitude']);
        $idade = $row['idade'] ?? null;

        if (isset($row['data_nascimento'])) {
            $idade = $idade ?? \Carbon\Carbon::parse($row['data_nascimento'])->age;
        }

        $paciente = Paciente::withTrashed()->where('numero_notificacao', $row['numero_notificacao'])->first() ?? new Paciente();
        $paciente->fill([
            'geom' => $point,
            'tipo' => $row['tipo'],
            'numero_notificacao' => $row['numero_notificacao'],
            'endereco' => $row['endereco'],
            'residencia' => $row['residencia'],
            'data_notificacao' => $row['data_notificacao'],
            'sexo' => $row['sexo'],
            'idade' => $idade,
            'faixa_etaria' => $row['faixa_etaria'],
            'condicao' => $row['condicao'],
            'data_alta_obito' => $row['data_alta_obito'],
            'status_exame' => $row['status_exame'],
            'data_coleta' => $row['data_coleta'],
            'agente_patogeno' => $row['agente_patogeno'],
            'id_municipio' => $bauru->id,
            'deleted_at' => null,
        ]);

        $paciente->save();

        unset($row['numero_notificacao'], $row['deleted_at']);

        // insere datas para o histórico
        foreach (range(1, 3) as $days) {
            $row['data_notificacao'] = \Carbon\Carbon::createFromFormat('Y-m-d', $row['data_notificacao'])->addDays(-$days)->toDateString();

            $paciente->historico()->updateOrCreate([
                'data_notificacao' => $row['data_notificacao'],
                'id_paciente' => $paciente->id,
            ], [
                'tipo' => $row['tipo'],
                'endereco' => $row['endereco'],
                'residencia' => $row['residencia'],
                'data_notificacao' => $row['data_notificacao'],
                'sexo' => $row['sexo'],
                'idade' => $idade,
                'faixa_etaria' => $row['faixa_etaria'],
                'condicao' => $row['condicao'],
                'data_alta_obito' => $row['data_alta_obito'],
                'status_exame' => $row['status_exame'],
                'data_coleta' => $row['data_coleta'],
                'agente_patogeno' => $row['agente_patogeno'],
                'id_municipio' => $bauru->id,
            ]);
        }

        return $paciente;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
