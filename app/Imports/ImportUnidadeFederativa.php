<?php

namespace App\Imports;

use App\Models\UnidadeFederativa;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportUnidadeFederativa implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return null|\Illuminate\Database\Eloquent\Model
     */
    public function model(array $row)
    {
        $dados = [
            'regiao' => Str::title($row['regiao']),
            'nome' => Str::title($row['uf']),
            'geocodigo' => $row['geocodigo'],
        ];

        return UnidadeFederativa::updateOrCreate([
            'geocodigo' => $dados['geocodigo'],
        ], $dados);
    }
}
