<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SeederJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $seederClassName;

    /**
     * Create a new job instance.
     */
    public function __construct(String $seederClassName)
    {
        $this->seederClassName = $seederClassName;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        \Artisan::call('db:seed --class=' . $this->seederClassName);

        gc_collect_cycles();
    }

    /**
     * The job failed to process.
     *
     * @param mixed $exception
     */
    public function failed($exception)
    {
        gc_collect_cycles();

        throw $exception;
    }
}
