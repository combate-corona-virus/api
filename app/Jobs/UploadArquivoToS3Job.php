<?php

namespace App\Jobs;

use Facades\App\Services\StorageService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UploadArquivoToS3Job implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $caminho;

    /**
     * Create a new job instance.
     */
    public function __construct(String $caminho)
    {
        $this->caminho = $caminho;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        StorageService::uploadToS3($this->caminho);
    }
}
