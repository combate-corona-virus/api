<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Arquivo extends Model
{
    use SoftDeletes;

    protected $table = 'arquivo';

    public $timestamps = true;

    protected $fillable = [
        'nome',
        'descricao',
        'caminho',
        'extensao',
        'mime',
        'tamanho',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //=========================== Mapping ===================================

    public function municipio(): HasOne
    {
        return $this->hasOne(Municipio::class, 'id_arquivo_logo');
    }

    //=========================== Accessors =================================

    public function getUrlAttribute()
    {
        $url = route(
            'arquivo.download',
            [
                'id' => $this->id,
            ]
        );

        return $url;
    }

    //=========================== Methods ===================================
}
