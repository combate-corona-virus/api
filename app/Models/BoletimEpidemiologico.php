<?php

namespace App\Models;

use App\Models\Scopes\UsuarioMunicipioScope;
use App\Models\Traits\OrderFromArgsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BoletimEpidemiologico extends Model
{
    use SoftDeletes,
        OrderFromArgsTrait;

    protected $table = 'boletim_epidemiologico';

    protected $dates = ['created_at', 'updated_at', 'data', 'deleted_at'];

    protected $fillable = [
        'data',
        'quantidade_suspeitos',
        'quantidade_confirmados',
        'quantidade_obitos',
        'id_municipio',
        'id_usuario',
        'quantidade_descartados',
        'quantidade_curados',
        'quantidade_obitos_suspeitos',
        'quantidade_confirmados_previsto_fiocruz',
        'quantidade_testagem',
        'quantidade_leitos_uti',
        'quantidade_leitos_uti_ocupados',
        'indice_isolamento_social',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UsuarioMunicipioScope());
    }

    //=========================== Mapping ===================================

    public function municipio()
    {
        return $this->belongsTo(Municipio::class, 'id_municipio');
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'id_usuario');
    }

    //=========================== Scopes ====================================

    //=========================== Accessors =================================

    public function getIncidenciaAttribute()
    {
        $populacao = $this->municipio()->select('populacao')->first()->populacao;
        $populacao = $populacao > 0 ? $populacao : 1;

        $quantidadeConfirmados = $this->quantidade_confirmados;

        return number_format(round(($quantidadeConfirmados / $populacao) * 100000, 2), 2);
    }

    public function getMortalidadeAttribute()
    {
        $populacao = $this->municipio()->select('populacao')->first()->populacao;
        $populacao = $populacao > 0 ? $populacao : 1;

        $quantidadeObitos = $this->quantidade_obitos;

        return number_format(round(($quantidadeObitos / $populacao) * 100000, 2), 2);
    }

    public function getLetalidadeAttribute()
    {
        $quantidadeConfirmados = $this->quantidade_confirmados;
        $quantidadeConfirmados = $quantidadeConfirmados > 0 ? $quantidadeConfirmados : 1;
        $quantidadeObitos = $this->quantidade_obitos;

        return number_format(round(($quantidadeObitos / $quantidadeConfirmados) * 100, 2), 2);
    }

    //=========================== Methods ===================================
}
