<?php

namespace App\Models;

use App\Enums\EnviadoPor;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use CastsEnums;

    protected $table = 'chat';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'enviado_por',
        'id_dispositivo',
        'nome_dispositivo',
        'texto',
        'id_municipio',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    protected $enumCasts = [
        'enviado_por' => EnviadoPor::class,
    ];

    //=========================== Mapping ===================================

    public function municipio()
    {
        return $this->belongsTo(Municipio::class, 'id_municipio');
    }

    //=========================== Scopes ====================================

    //=========================== Accessors =================================

    //=========================== Methods ===================================
}
