<?php

namespace App\Models;

use App\Enums\NomeGeocamada;
use App\Enums\PolymorphicType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

class Geocamada extends Model
{
    use SoftDeletes;

    protected $table = 'geocamada';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'id_geocamada_pai',
        'nome',
        'nome_legivel',
        'cor',
        'icone',
        'icone_unicode',
        'is_custom',
        'query',
        'indice',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'query' => 'json',
    ];

    protected $enumCasts = [
        'nome ' => NomeGeocamada::class,
    ];

    //=========================== Mapping ===================================

    public function geocamadaPai()
    {
        return $this->belongsTo(self::class, 'id_geocamada_pai');
    }

    public function geocamadasFilhas()
    {
        return $this->hasMany(self::class, 'id_geocamada_pai');
    }

    public function pacientes()
    {
        return $this->morphedByMany(Paciente::class, PolymorphicType::POLYMORPHIC_FIELD_NAME()->value, 'tabela_geocamada', 'id_geocamada');
    }

    public function tabelasGeocamada()
    {
        return $this->hasMany(TabelaGeocamada::class, 'id_geocamada');
    }

    //=========================== Accessors =================================

    //=========================== Scopes ===================================

    /* Scope pra verificar se a geocamada é pai
    *
    * @param  \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder  $builder
    * @param  mixed  $value
    * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
    */
    public function scopeIsPai($builder, $isPai)
    {
        return $isPai ? $builder->whereNull('id_geocamada_pai') : $builder->whereNotNull('id_geocamada_pai');
    }

    //=========================== Methods ===================================
    public function getId(String $nome)
    {
        return $this->whereNome($nome)->select('id')->firstOrFail()->id;
    }

    /**
     * retorna mapeamento de geocamadas de acordo com a classificação do paciente.
     *
     * @param Paciente $paciente
     *
     * @return Collection
     */
    public function getGeocamadaPacienteMap(Paciente $paciente): Collection
    {
        return collect([
            NomeGeocamada::CONFIRMADO()->value => $paciente->isCovid19Confirmado(),
            NomeGeocamada::SUSPEITO_EM_ANALISE()->value => $paciente->isSuspeito(),
            NomeGeocamada::DESCARTADO_OUTROS()->value => $paciente->isDescartado(),
            NomeGeocamada::ALTA_LIBERADO_DO_ISOLAMENTO()->value => $paciente->isAltaLiberadoIsolamento(),
            NomeGeocamada::ALTA_ISOLAMENTO_SOCIAL()->value => $paciente->isAltaIsolamentoSocial(),
            NomeGeocamada::EM_ACOMPANHAMENTO_ESTAVEL()->value => $paciente->isEmAcompanhamentoEstavel(),
            NomeGeocamada::EM_ACOMPANHAMENTO_GRAVE()->value => $paciente->isEmAcompanhamentoGrave(),
            NomeGeocamada::OBITO()->value => $paciente->isObito(),
            NomeGeocamada::MAPA_CALOR_INCIDENCIA_DE_CONFIRMADOS()->value => $paciente->isCovid19Confirmado(),
            NomeGeocamada::MAPA_CALOR_INCIDENCIA_DE_SUSPEITAS()->value => $paciente->isSuspeito(),
        ]);
    }

    /**
     * Retorna IDs das geocamadas que um paciente pode se associar.
     *
     * @param Paciente $paciente
     *
     * @return array
     */
    public function getIdsGeocamadasFromPaciente(Paciente $paciente): array
    {
        $geocamadaPacienteMap = $this->getGeocamadaPacienteMap($paciente);

        $geocamadasCorrespondentes = $geocamadaPacienteMap->filter(function ($value) {
            return $value;
        })->keys()->all();

        return $this->whereIn('nome', $geocamadasCorrespondentes)->pluck('id')->all();
    }
}
