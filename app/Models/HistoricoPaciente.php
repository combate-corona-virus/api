<?php

namespace App\Models;

use App\Enums\PacienteAgentePatogeno;
use App\Enums\PacienteCondicao;
use App\Enums\PacienteFaixaEtaria;
use App\Enums\PacienteStatusExame;
use App\Enums\PacienteTipo;
use App\Enums\PolymorphicType;
use App\Models\Scopes\UsuarioMunicipioScope;
use App\Models\Traits\OrderFromArgsTrait;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use MStaack\LaravelPostgis\Eloquent\PostgisTrait;

class HistoricoPaciente extends Model
{
    use CastsEnums,
        OrderFromArgsTrait,
        PostgisTrait;

    protected $table = 'historico_paciente';

    protected $dates = ['created_at', 'updated_at', 'data_alta_obito', 'data_coleta', 'data_notificacao'];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'id_municipio',
        'id_paciente',
        'tipo',
        'endereco',
        'residencia',
        'data_notificacao',
        'sexo',
        'idade',
        'faixa_etaria',
        'condicao',
        'data_alta_obito',
        'status_exame',
        'data_coleta',
        'agente_patogeno',
        'geom',
    ];

    protected $postgisFields = [
        'geom',
    ];

    protected $postgisTypes = [
        'geom' => [
            'geomtype' => 'geometry',
            'srid' => 4326,
        ],
    ];

    protected $enumCasts = [
        'tipo' => PacienteTipo::class,
        'condicao' => PacienteCondicao::class,
        'faixa_etaria' => PacienteFaixaEtaria::class,
        'status_exame' => PacienteStatusExame::class,
        'agente_patogeno' => PacienteAgentePatogeno::class,
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UsuarioMunicipioScope());
    }

    //=========================== Mapping ===================================
    public function municipio()
    {
        return $this->belongsTo(Municipio::class, 'id_municipio');
    }

    public function tabelaGeocamada()
    {
        return $this->morphMany(TabelaGeocamada::class, PolymorphicType::POLYMORPHIC_FIELD_NAME()->value);
    }

    public function geocamada()
    {
        return $this->morphToMany(Geocamada::class, PolymorphicType::POLYMORPHIC_FIELD_NAME()->value, 'tabela_geocamada', 'tabela_id', 'id_geocamada');
    }

    public function paciente()
    {
        return $this->belongsTo(Paciente::class, 'id_paciente');
    }

    //=========================== Scopes ====================================

    public function scopeIsConfirmado($builder)
    {
        return $builder->where('agente_patogeno', PacienteAgentePatogeno::COVID19()->value)
            ->where('status_exame', PacienteStatusExame::CONCLUIDO()->value)
            ->where('condicao', '<>', PacienteCondicao::OBITO()->value);
    }

    public function scopeIsObito($builder)
    {
        return $builder->where('agente_patogeno', PacienteAgentePatogeno::COVID19()->value)
            ->where('status_exame', PacienteStatusExame::CONCLUIDO()->value)
            ->where('condicao', PacienteCondicao::OBITO()->value);
    }

    public function scopeIsDescartado($builder)
    {
        return $builder->where('status_exame', PacienteStatusExame::CONCLUIDO()->value)
            ->where('agente_patogeno', '<>', PacienteAgentePatogeno::COVID19()->value);
    }

    public function scopeIsSuspeito($builder)
    {
        return $builder->where('status_exame', PacienteStatusExame::COLETADO()->value)
            ->where('agente_patogeno', '<>', PacienteAgentePatogeno::COVID19()->value)
            ->orWhere(function ($query) {
                $query->where('status_exame', PacienteStatusExame::COLETADO()->value)
                    ->whereIsNull('agente_patogeno');
            });
    }

    //=========================== Accessors =================================

    //=========================== Methods ===================================
}
