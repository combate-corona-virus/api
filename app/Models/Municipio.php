<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MStaack\LaravelPostgis\Eloquent\PostgisTrait;

class Municipio extends Model
{
    use PostgisTrait,
        SoftDeletes;

    protected $table = 'municipio';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'id_unidade_federativa',
        'nome',
        'geocodigo',
        'populacao',
        'geom',
        'geom_centro',
        'is_participante',
        'id_regiao_administrativa',
        'id_arquivo_logo',
        'texto_contato',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $postgisFields = [
        'geom',
        'geom_centro',
    ];

    protected $postgisTypes = [
        'geom' => [
            'geomtype' => 'geometry',
            'srid' => 4326,
        ],
        'geom_centro' => [
            'geomtype' => 'geometry',
            'srid' => 4326,
        ],
    ];

    private const GEOCODIGO_BAURU = 3506003;
    private const NOME_BAURU = 'Bauru';

    //=========================== Mapping ===================================

    public function usuarios()
    {
        return $this->hasMany(Usuario::class, 'id_municipio');
    }

    public function boletimEpidemiologico()
    {
        return $this->hasMany(BoletimEpidemiologico::class, 'id_municipio');
    }

    public function pacientes()
    {
        return $this->hasMany(Paciente::class, 'id_municipio');
    }

    public function unidadeFederativa()
    {
        return $this->belongsTo(UnidadeFederativa::class, 'id_unidade_federativa');
    }

    public function regiaoAdministrativa()
    {
        return $this->belongsTo(RegiaoAdministrativa::class, 'id_regiao_administrativa');
    }

    public function chats()
    {
        return $this->hasMany(Chat::class, 'id_municipio');
    }

    public function logo()
    {
        return $this->belongsTo(Arquivo::class, 'id_arquivo_logo');
    }

    //=========================== Scopes ====================================

    public function scopeBounded($builder, $bounds)
    {
        $bounds = (object) $bounds;

        $envelope = "ST_MakeEnvelope($bounds->x_min, $bounds->y_min, $bounds->x_max, $bounds->y_max, 4326)";

        return $builder->whereRaw("ST_Intersects(geom, $envelope)");
    }

    public function scopeIsBauru($builder, $isBauru = true)
    {
        return $isBauru ? $builder->where('geocodigo', self::GEOCODIGO_BAURU)->orWhere('nome', self::NOME_BAURU) : $builder;
    }

    public function scopeCoordinates($builder, $coordenadas)
    {
        if (!isset($coordenadas['latitude']) || !isset($coordenadas['longitude'])) {
            return $builder;
        }

        return $builder->whereRaw("ST_Intersects(ST_SetSRID(ST_Point({$coordenadas['longitude']},{$coordenadas['latitude']}),4326),geom)");
    }

    //=========================== Accessors =================================

    public function getUltimoBoletimEpidemiologicoAttribute()
    {
        return $this->boletimEpidemiologico()->latest()->first();
    }

    public function getNomePrefeituraAttribute()
    {
        return "Prefeitura Municipal de {$this->nome}";
    }

    //=========================== Methods ===================================
}
