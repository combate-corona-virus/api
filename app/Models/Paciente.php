<?php

namespace App\Models;

use App\Enums\PacienteAgentePatogeno;
use App\Enums\PacienteCondicao;
use App\Enums\PacienteFaixaEtaria;
use App\Enums\PacienteStatusExame;
use App\Enums\PacienteTipo;
use App\Enums\PolymorphicType;
use App\Models\Scopes\UsuarioMunicipioScope;
use App\Models\Traits\OrderFromArgsTrait;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MStaack\LaravelPostgis\Eloquent\PostgisTrait;

class Paciente extends Model
{
    use SoftDeletes,
        CastsEnums,
        OrderFromArgsTrait,
        PostgisTrait;

    protected $table = 'paciente';

    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'data_alta_obito', 'data_coleta', 'data_notificacao'];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'id_municipio',
        'tipo',
        'numero_notificacao',
        'endereco',
        'residencia',
        'data_notificacao',
        'sexo',
        'idade',
        'faixa_etaria',
        'condicao',
        'data_alta_obito',
        'status_exame',
        'data_coleta',
        'agente_patogeno',
        'geom',
        'deleted_at',
    ];

    protected $postgisFields = [
        'geom',
    ];

    protected $postgisTypes = [
        'geom' => [
            'geomtype' => 'geometry',
            'srid' => 4326,
        ],
    ];

    protected $enumCasts = [
        'tipo' => PacienteTipo::class,
        'condicao' => PacienteCondicao::class,
        'faixa_etaria' => PacienteFaixaEtaria::class,
        'status_exame' => PacienteStatusExame::class,
        'agente_patogeno' => PacienteAgentePatogeno::class,
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UsuarioMunicipioScope());
    }

    //=========================== Mapping ===================================

    public function municipio()
    {
        return $this->belongsTo(Municipio::class, 'id_municipio');
    }

    public function tabelaGeocamada()
    {
        return $this->morphMany(TabelaGeocamada::class, PolymorphicType::POLYMORPHIC_FIELD_NAME()->value);
    }

    public function geocamada()
    {
        return $this->morphToMany(Geocamada::class, PolymorphicType::POLYMORPHIC_FIELD_NAME()->value, 'tabela_geocamada', 'tabela_id', 'id_geocamada');
    }

    public function historico()
    {
        return $this->hasMany(HistoricoPaciente::class, 'id_paciente');
    }

    //=========================== Scopes ====================================

    public function scopeBounded($builder, $bounds)
    {
        $bounds = (object) $bounds;

        $envelope = "ST_MakeEnvelope($bounds->x_min, $bounds->y_min, $bounds->x_max, $bounds->y_max, 4326)";

        return $builder->whereRaw("ST_Intersects(geom, $envelope)");
    }

    //=========================== Accessors =================================

    //=========================== Methods ===================================

    public function isCovid19Confirmado(): bool
    {
        return $this->agente_patogeno
            && $this->agente_patogeno->is(PacienteAgentePatogeno::COVID19)
            && $this->status_exame && $this->status_exame->is(PacienteStatusExame::CONCLUIDO)
            && (!$this->condicao || $this->condicao->isNot(PacienteCondicao::OBITO));
    }

    public function isSuspeito(): bool
    {
        return !$this->isCovid19Confirmado()
            && $this->status_exame
            && $this->status_exame->is(PacienteStatusExame::COLETADO);
    }

    public function isObito(): bool
    {
        return $this->agente_patogeno
            && $this->agente_patogeno->is(PacienteAgentePatogeno::COVID19)
            && $this->status_exame && $this->status_exame->is(PacienteStatusExame::CONCLUIDO)
            && $this->condicao && $this->condicao->is(PacienteCondicao::OBITO);
    }

    public function isDescartado(): bool
    {
        return $this->status_exame
            && $this->status_exame->is(PacienteStatusExame::CONCLUIDO)
            && (!$this->agente_patogeno || $this->agente_patogeno->isNot(PacienteAgentePatogeno::COVID19));
    }

    public function isAltaLiberadoIsolamento(): bool
    {
        return $this->condicao
            && $this->condicao->is(PacienteCondicao::ALTA_LIBERADO_DO_ISOLAMENTO);
    }

    public function isAltaIsolamentoSocial(): bool
    {
        return $this->condicao
            && $this->condicao->is(PacienteCondicao::ALTA_ISOLAMENTO_SOCIAL);
    }

    public function isEmAcompanhamentoEstavel(): bool
    {
        return $this->condicao
            && $this->condicao->is(PacienteCondicao::EM_ACOMPANHAMENTO_ESTAVEL);
    }

    public function isEmAcompanhamentoGrave(): bool
    {
        return $this->condicao
            && $this->condicao->is(PacienteCondicao::EM_ACOMPANHAMENTO_GRAVE);
    }

    public function attachGeocamadasCorrespondentes(): void
    {
        $geocamada = \App::make(Geocamada::class);
        $idsGeocamadas = $geocamada->getIdsGeocamadasFromPaciente($this);

        $this->geocamada()->sync($idsGeocamadas);
    }

    public function createHistorico(): void
    {
        $historicoData = $this->toArray();

        unset($historicoData['numero_notificacao'], $historicoData['id'], $historicoData['deleted_at']);

        $this->historico()->updateOrCreate([
            'data_notificacao' => $this->data_notificacao,
            'id_paciente' => $this->id,
        ], $historicoData);
    }

    public function setGeomCentroMunicipioIfMissing()
    {
        $this->geom = $this->geom ?? Municipio::find($this->id_municipio)->select('geom_centro')->first()->geom_centro;

        return $this;
    }
}
