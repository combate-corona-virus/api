<?php

namespace App\Models;

use Laratrust\Models\LaratrustPermission;

class Permission extends LaratrustPermission
{
    protected $fillable = [
        'name',
        'display_name',
        'description',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
