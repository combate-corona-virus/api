<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegiaoAdministrativa extends Model
{
    protected $table = 'regiao_administrativa';

    protected $fillable = ['nome'];

    protected $dates = ['created_at', 'updated_at'];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    //=========================== Mapping ===================================

    public function municipio()
    {
        return $this->hasMany(Municipio::class, 'id_regiao_administrativa');
    }

    //=========================== Accessors =================================
    //=========================== Scopes ===================================
    //=========================== Methods ===================================
}
