<?php

namespace App\Models;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    protected $fillable = [
        'name',
        'display_name',
        'description',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
