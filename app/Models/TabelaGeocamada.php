<?php

namespace App\Models;

use App\Enums\PolymorphicType;
use Illuminate\Database\Eloquent\Model;

class TabelaGeocamada extends Model
{
    protected $table = 'tabela_geocamada';

    protected $fillable = ['id_geocamada', 'tabela_id', 'tabela_type'];

    protected $dates = ['created_at', 'updated_at'];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    //=========================== Mapping ===================================

    public function geocamada()
    {
        return $this->belongsTo(Geocamada::class, 'id_geocamada');
    }

    public function geoElemento()
    {
        return $this->morphTo(PolymorphicType::POLYMORPHIC_FIELD_NAME()->value);
    }

    //=========================== Accessors =================================

    //=========================== Scopes ===================================
    //=========================== Methods ===================================
}
