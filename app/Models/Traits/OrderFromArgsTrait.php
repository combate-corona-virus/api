<?php

namespace App\Models\Traits;

trait OrderFromArgsTrait
{
    /**
     * Apply an "ORDER BY" clause.
     *
     * @param  \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder  $builder
     * @param  mixed  $orderArgs
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function scopeOrderFromArgs($builder, $orderArgs)
    {
        if ($orderArgs) {
            foreach ($orderArgs as $orderByClause) {
                $builder->orderBy(
                    $orderByClause['field'],
                    $orderByClause['order']
                );
            }
        }

        return $builder;
    }
}
