<?php

namespace App\Models\Traits;

use Illuminate\Support\Str;

trait UrlTrait
{
    public function getClientUrlAttribute()
    {
        $classBaseName = Str::slug(Str::snake(class_basename($this)));
        $clientPrefix = '#';

        return url("/$clientPrefix/$classBaseName", [$this->getKey()]);
    }
}
