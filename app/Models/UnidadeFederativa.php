<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use MStaack\LaravelPostgis\Eloquent\PostgisTrait;

class UnidadeFederativa extends Model
{
    use PostgisTrait,
        SoftDeletes;

    protected $table = 'unidade_federativa';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'regiao',
        'nome',
        'geocodigo',
        'geom',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $postgisFields = [
        'geom',
    ];

    protected $postgisTypes = [
        'geom' => [
            'geomtype' => 'geometry',
            'srid' => 4326,
        ],
    ];

    //=========================== Mapping ===================================

    public function municipio()
    {
        return $this->hasMany(self::class, 'id_unidade_federativa');
    }
}
