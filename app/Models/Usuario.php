<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Laravel\Passport\HasApiTokens;

class Usuario extends Authenticatable
{
    use
        LaratrustUserTrait,
        AuthenticableTrait,
        HasApiTokens,
        SoftDeletes,
        Notifiable;

    protected $table = 'usuario';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'nome',
        'sobrenome',
        'email',
        'password',
        'username',
        'id_municipio',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //=========================== Mapping ===================================

    public function municipio()
    {
        return $this->belongsTo(Municipio::class, 'id_municipio');
    }

    //=========================== Scopes ====================================

    //=========================== Accessors =================================

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = app('hash')->make($password);
    }

    //=========================== Methods ===================================

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'usuario.' . $this->id;
    }
}
