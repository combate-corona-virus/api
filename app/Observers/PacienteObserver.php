<?php

namespace App\Observers;

use App\Models\Paciente;

class PacienteObserver
{
    /**
     * Handle the paciente "creating" event.
     *
     * @param  \App\Models\Paciente  $paciente
     */
    public function creating(Paciente $paciente)
    {
        //$paciente->setGeomCentroMunicipioIfMissing();
    }

    /**
     * Handle the paciente "created" event.
     *
     * @param  \App\Models\Paciente  $paciente
     */
    public function created(Paciente $paciente)
    {
        $paciente->attachGeocamadasCorrespondentes();
        $paciente->createHistorico();
    }

    /**
     * Handle the paciente "updating" event.
     *
     * @param  \App\Models\Paciente  $paciente
     */
    public function updating(Paciente $paciente)
    {
        //$paciente->setGeomCentroMunicipioIfMissing();
    }

    /**
     * Handle the paciente "updated" event.
     *
     * @param  \App\Models\Paciente  $paciente
     */
    public function updated(Paciente $paciente)
    {
        $paciente->attachGeocamadasCorrespondentes();
        $paciente->createHistorico();
    }

    /**
     * Handle the paciente "deleted" event.
     *
     * @param  \App\Models\Paciente  $paciente
     */
    public function deleted(Paciente $paciente)
    {
    }

    /**
     * Handle the paciente "restored" event.
     *
     * @param  \App\Models\Paciente  $paciente
     */
    public function restored(Paciente $paciente)
    {
    }

    /**
     * Handle the paciente "force deleted" event.
     *
     * @param  \App\Models\Paciente  $paciente
     */
    public function forceDeleted(Paciente $paciente)
    {
    }
}
