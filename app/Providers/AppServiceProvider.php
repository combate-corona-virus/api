<?php

namespace App\Providers;

use App\Enums\PolymorphicType;
use App\Models\Paciente;
use App\Observers\PacienteObserver;
use App\Services\AuthService;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        Relation::morphMap([
            PolymorphicType::PACIENTE()->value => Paciente::class,
        ]);

        Paciente::observe(PacienteObserver::class);
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        $this->app->singleton(AuthService::class, function () {
            return new AuthService();
        });

        $baseUrl = config('services.googlemaps.baseurl');

        $this->app->singleton(Client::class, function ($api) use ($baseUrl) {
            return new Client([
                'base_uri' => $baseUrl,
            ]);
        });

        $this->app->singleton(\App\Helpers\FeaturesHelper::class, function ($app) {
            return new \App\Helpers\FeaturesHelper();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [AuthService::class];
    }
}
