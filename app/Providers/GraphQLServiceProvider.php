<?php

namespace App\Providers;

use App\Enums\EnviadoPor;
use App\Enums\NomeGeocamada;
use App\Enums\PacienteAgentePatogeno;
use App\Enums\PacienteCondicao;
use App\Enums\PacienteFaixaEtaria;
use App\Enums\PacienteStatusExame;
use App\Enums\PacienteTipo;
use App\Enums\TipoCaso;
use Illuminate\Support\ServiceProvider;
use Nuwave\Lighthouse\Schema\TypeRegistry;
use Nuwave\Lighthouse\Schema\Types\LaravelEnumType;

class GraphQLServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param  \Nuwave\Lighthouse\Schema\TypeRegistry  $typeRegistry
     */
    public function boot(TypeRegistry $typeRegistry): void
    {
        $typeRegistry->register(new LaravelEnumType(NomeGeocamada::class));
        $typeRegistry->register(new LaravelEnumType(TipoCaso::class));
        $typeRegistry->register(new LaravelEnumType(PacienteTipo::class));
        $typeRegistry->register(new LaravelEnumType(PacienteFaixaEtaria::class));
        $typeRegistry->register(new LaravelEnumType(PacienteCondicao::class));
        $typeRegistry->register(new LaravelEnumType(PacienteStatusExame::class));
        $typeRegistry->register(new LaravelEnumType(PacienteAgentePatogeno::class));
        $typeRegistry->register(new LaravelEnumType(EnviadoPor::class));
    }
}
