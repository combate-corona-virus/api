<?php

namespace App\Services;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

class AuthService
{
    /**
     * Login do usuário.
     *
     * @param string $email
     * @param string $password
     * @param bool $remember
     *
     * @return mixed
     */
    public function login(string $email, string $password, bool $remember)
    {
        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
            $user = Auth::user();

            $user['auth_token'] = $user->createToken('auth_token')->accessToken;

            return $user;
        }

        throw new AuthenticationException();
    }

    /**
     * força o logout do usuário removendo seu token de acesso a api.
     *
     * @return bool
     */
    public function logout()
    {
        $user = Auth::user();

        $userTokens = $user->tokens;
        foreach ($userTokens as $token) {
            $token->revoke();
        }

        return true;
    }
}
