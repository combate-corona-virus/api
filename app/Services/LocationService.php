<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log as Log;
use MStaack\LaravelPostgis\Geometries\Point;

class LocationService
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    private function get(string $url, array $params)
    {
        $getQueryStringParams = $this->getQueryStringParams($params);

        try {
            $response = $this->client->request('GET', $url, [
                'query' => $getQueryStringParams,
            ]);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());

            return null;
        }

        return json_decode($response->getBody()->getContents());
    }

    private function getQueryStringParams(array $params)
    {
        $params['key'] = config('services.googlemaps.key');

        return $params;
    }

    /**
     * ecupera Point atravez do endereço.
     *
     * @param string $endereco
     *
     * @return null|Point
     */
    public function getPointFromEndereco(String $endereco)
    {
        $response = $this->get('', [
            'address' => $endereco,
        ]);

        $result = $response->results[0] ?? null;

        $geometry = $result->geometry ?? null;

        if (isset($geometry->location)) {
            return new Point($geometry->location->lat, $geometry->location->lng);
        }

        return null;
    }
}
