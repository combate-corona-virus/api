<?php

namespace App\Services;

use App\Enums\ArquivoBasePath;
use App\Enums\CodigoTipoAnexoCadastroInteressado;
use App\Exceptions\InvalidFileException;
use App\Jobs\UploadArquivoToS3Job;
use App\Models\Arquivo;
use App\Models\CadastroInteressado;
use App\Models\Municipio;
use App\Models\TipoAnexoCadastroInteressado;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManager;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\UploadedFile as SymfonyUploadedFile;

class StorageService
{
    public $imageManager;

    public function __construct(ImageManager $imageManager)
    {
        $this->imageManager = $imageManager;
    }

    /**
     * Salva a imagem no local storage e cria uma entrada na tabela de arquivos.
     *
     * @param Arquivo $arquivo
     * @param SymfonyUploadedFile $file
     * @param mixed $basePath
     *
     * @return Arquivo
     */
    public function storeImagem(SymfonyUploadedFile $file, $basePath = ArquivoBasePath::IMAGEM_ORIGINAL): Arquivo
    {
        $this->validarImagem($file);

        $tmpPath = 'tmp/' . Uuid::uuid4()->toString();

        //todo job pra fazer o resize
        $image = $this->imageManager->make($file);
        $image->resize(300, 100, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        Storage::disk('local')->put($tmpPath, $image->stream()->__toString());

        $file = new SymfonyUploadedFile(storage_path('app/' . $tmpPath), $file->getClientOriginalName(), $file->getMimeType(), null, true);

        $arquivo = $this->storeArquivo($file, $basePath);
        $image->destroy();

        return $arquivo;
    }

    /**
     * Salva a imagens no local storage e cria uma entrada na tabela de arquivos para cada arquivo.
     *
     * @param Arquivo $arquivo
     * @param array $files
     */
    public function storeImagens(array $files)
    {
        $extractedFiles = $this->extractFiles($files);

        foreach ($extractedFiles as $file) {
            $this->storeImagem($file);
        }
    }

    /**
     * Salva arquivos no local storage e cria uma entrada na tabela de arquivos para cada arquivo.
     *
     * @param array $files
     *
     * @return array
     */
    public function storeArquivos(array $files): array
    {
        $arquivos = [];
        $extractedFiles = $this->extractFiles($files);

        foreach ($extractedFiles as $file) {
            array_push($arquivos, $this->storeArquivo($file));
        }

        return $arquivos;
    }

    /**
     * Salva arquivos no local storage e cria uma entrada na tabela de arquivos para cada arquivo.
     *
     * @param array $files
     * @param int $id_cadastro_interessado
     * @param int $id_tipo_anexo_cadastro_interessado
     *
     * @return bool
     */
    public function storeArquivosCadastroInteressado(array $files, int $idCadastroInteressado, int $idTipoAnexoCadastroInteressado): bool
    {
        $cadastroInteressado = CadastroInteressado::findOrFail($idCadastroInteressado);
        $extractedFiles = $this->extractFiles($files);

        foreach ($extractedFiles as $file) {
            $id = $this->storeArquivo($file)->id;
            $cadastroInteressado->arquivo()->attach($id, ['id_tipo_anexo_cadastro_interessado' => $idTipoAnexoCadastroInteressado]);
        }

        return true;
    }

    /**
     * Salva arquivos do cadastro de interessado pelo nome do tipo de anexo.
     *
     * @param array $files
     * @param int $id_cadastro_interessado
     * @param int $id_tipo_anexo_cadastro_interessado
     *
     * @return bool
     */
    public function storeArquivosCadastroInteressadoByCodigoTipoAnexo(array $files, int $idCadastroInteressado, string $codigoTipoAnexo): bool
    {
        $codigoTipoAnexo = CodigoTipoAnexoCadastroInteressado::coerce(Str::upper($codigoTipoAnexo));

        $tipoAnexo = TipoAnexoCadastroInteressado::where('codigo', $codigoTipoAnexo->value)->first();

        return $this->storeArquivosCadastroInteressado($files, $idCadastroInteressado, $tipoAnexo->id);
    }

    /**
     * Salva o arquivo no local storage e cria uma entrada na tabela de arquivos.
     *
     * @param Arquivo $arquivo
     * @param SymfonyUploadedFile $file
     * @param string $basePath
     *
     * @return Arquivo
     */
    public function storeArquivo(SymfonyUploadedFile $file, String $basePath = ArquivoBasePath::
    DEFAULT): Arquivo
    {
        try {
            $this->validarArquivo($file);

            $extensao = $this->getExtensao($file);

            $caminho = $this->storeLocalFile($file, $basePath);

            $arquivo = Arquivo::create([
                'nome' => $file->getClientOriginalName(),
                'caminho' => $caminho,
                'extensao' => $extensao,
                'mime' => $file->getMimeType(),
                'tamanho' => $file->getSize(),
            ]);

            // dispara o job pra fazer o upload do arquivo pra s3
            UploadArquivoToS3Job::dispatch($caminho);

            return $arquivo;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Salva o arquivo no disco local.
     *
     * @param SymfonyUploadedFile $file
     * @param string $basePath
     */
    public function storeLocalFile(SymfonyUploadedFile $file, String $basePath = ArquivoBasePath::
    DEFAULT)
    {
        $extensao = $this->getExtensao($file);

        $hashArquivo = Uuid::uuid4()->toString() . '.' . $extensao;

        $basePathEnum = $basePath ? ArquivoBasePath::getInstance($basePath) : ArquivoBasePath::getInstance(ArquivoBasePath::
        DEFAULT);

        $prefix = $this->getStoragePathPrefix();
        $basePath = $prefix . $basePathEnum->value;

        $uploadedFile = UploadedFile::createFromBase($file);
        $caminho = Storage::disk('local')->putFileAs($basePath, $uploadedFile, $hashArquivo);

        return $caminho;
    }

    /**
     * Valida imagem.
     *
     * @param SymfonyUploadedFile $file
     */
    public function validarImagem(SymfonyUploadedFile $file)
    {
        if (!$file->isValid()) {
            throw new InvalidFileException($file->getError());
        }

        $extensao = $this->getExtensao($file);

        $extensoesPermitidas = config('upload.extensoes.permitidas.imagem');

        if (!in_array($extensao, $extensoesPermitidas)) {
            $joinExtensoesPermitidas = implode(',', $extensoesPermitidas);

            throw new InvalidFileException("A extensão {$extensao} não é permitida. \n As extensões suportadas são: $joinExtensoesPermitidas");
        }
    }

    /**
     * Valida arquivo.
     *
     * @param SymfonyUploadedFile $file
     */
    public function validarArquivo(SymfonyUploadedFile $file)
    {
        if (!$file->isValid()) {
            throw new InvalidFileException($file->getError());
        }

        $extensao = $this->getExtensao($file);

        $extensoesBloqueadas = config('upload.extensoes.bloqueadas');

        if (in_array($extensao, $extensoesBloqueadas)) {
            throw new InvalidFileException("A extensão {$extensao} não é permitida.");
        }
    }

    /**
     * Retorna extensão do arquivo.
     *
     * @param SymfonyUploadedFile $file
     *
     * @return string
     */
    public function getExtensao(SymfonyUploadedFile $file)
    {
        return $file->getClientOriginalExtension() ?? $file->guessClientExtension();
    }

    /**
     * Realiza o download do arquivo.
     *
     * @param int $id
     */
    public function downloadArquivoById(int $id)
    {
        $arquivo = Arquivo::findOrFail($id);

        return $this->downloadArquivo($arquivo->caminho, $arquivo->mime, $arquivo->nome, $arquivo->tamanho);
    }

    public function downloadFromLocalStorage($path, $fileName)
    {
        return response()->download(storage_path($path), $fileName);
    }

    /**
     * realiza o download do arquivo.
     *
     * @param string $caminho
     * @param string $mime
     * @param string $nome
     * @param int $tamanho
     */
    public function downloadArquivo(String $caminho, String $mime = null, String $nome = null, int $tamanho = null)
    {
        $disk = 'local';

        if (Storage::disk('local')->exists($caminho)) {
            $disk = 'local';
        } elseif (Storage::disk('s3')->exists($caminho)) {
            $disk = 's3';
        } else {
            throw new FileNotFoundException($caminho);
        }

        $storage = Storage::disk($disk); //Storage::getDriver();
        $tamanho = $tamanho ?? $storage->size($caminho);
        $nome = $nome ?? basename($caminho);

        $headers = [
            'Cache-Control' => 'public',
            'Content-Length' => $tamanho,
            'Content-Description' => 'File Transfer',
            'Content-Disposition' => 'attachment; filename=' . $nome,
            'Content-Transfer-Encoding' => 'binary',
        ];

        if ($mime) {
            $headers['Content-Type'] = $mime;
        }

        return Storage::download($caminho, $nome, $headers);
    }

    public function deleteArquivo(Arquivo $arquivo): bool
    {
        $disk = 'local';
        if (!Storage::disk('local')->exists($arquivo->caminho)) {
            if (Storage::disk('s3')->exists($arquivo->caminho)) {
                $disk = 's3';
            } else {
                throw new FileNotFoundException($arquivo->caminho);
            }
        }

        Storage::disk($disk)->delete($arquivo->caminho);

        $arquivo->delete();

        return true;
    }

    public function deleteByIdArquivo($idArquivo): bool
    {
        $arquivo = Arquivo::find($idArquivo);

        if ($arquivo) {
            return $this->deleteArquivo($arquivo);
        }

        return false;
    }

    /**
     * Undocumented function.
     *
     * @param string $caminho
     */
    public function uploadToS3(String $caminho)
    {
        if (!Storage::disk('local')->exists($caminho)) {
            throw new FileNotFoundException($caminho);
        }

        // se o driver default for s3, faz o upload pra s3 e deleta o arquivo local
        // caso contrário não faz nada, pois o arquivo já se encontra no storage local
        if (config('filesystem.default') == config('filesystem.disks.s3.driver')) {
            $file = new File(storage_path('app/' . $caminho));

            $caminhoArray = explode('/', $caminho);
            $nomeArquivo = array_pop($caminhoArray);
            $caminho = implode('/', $caminhoArray);

            Storage::disk('s3')->putFileAs($caminho, $file, $nomeArquivo);
            Storage::disk('local')->delete($caminho);
        }
    }

    /**
     * Undocumented function.
     *
     * @param array $files
     *
     * @return array $filteredFiles
     */
    public function extractFiles(array $files)
    {
        $filesColletion = collect($files);

        $filteredFiles = $filesColletion->flatMap(function ($file) {
            return $file;
        })->filter(function ($file) {
            return $file instanceof SymfonyUploadedFile;
        })->all();

        return $filteredFiles;
    }

    /**
     * Retorna o prefixo da pasta a ser usada pra salvar os arquivos.
     *
     * @return string
     */
    public function getStoragePathPrefix()
    {
        return config('filesystems.prefix') . '/';
    }

    /**
     * Salva a logo do municipio.
     *
     * @param mixed $basePath
     *
     * @return bool
     */
    public function storeLogoMunicipio(int $idMunicipio, SymfonyUploadedFile $file)
    {
        $municipio = Municipio::findOrFail($idMunicipio);

        $arquivo = $this->storeImagem($file, ArquivoBasePath::IMAGEM_LOGO);

        $logoAtual = $municipio->logo;
        if ($logoAtual) {
            $municipio->update(['id_arquivo_logo' => null]);
            $this->deleteArquivo($logoAtual);
        }
        $municipio->update(['id_arquivo_logo' => $arquivo->id]);

        return $arquivo;
    }
}
