<?php

return [
    'channels' => [
        'default' => env('SLACK_DEFAULT_CHANNEL'),
    ],
    'icon' => ':laravel:',
    'from' => env('APP_NAME', 'Laravel'),
    'webhook' => env('SLACK_WEBHOOK'),
];
