<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Enums\EnviadoPor;
use App\Models\Chat;
use App\Models\Municipio;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Chat::class, function (Faker $faker) {
    $idDevices = [
        '2d40b2c0-6d68-470f-8e32-b59aeb1ada34',
        '2b85fdee-5e67-4a22-a266-d9c74042dbc2',
        '37267bad-0e94-4c5d-b554-705ab0a18ace',
    ];

    return [
        'enviado_por' => $faker->randomElement(EnviadoPor::getValues()),
        'id_dispositivo' => $faker->randomElement($idDevices),
        'nome_dispositivo' => $faker->randomElement(['IOS, Android']),
        'texto' => $faker->text(200),
        'created_at' => Carbon::now()->addSeconds(rand(1, 60)),
    ];
});

$factory->state(Chat::class, 'bauru', function (Faker $faker) {
    return [
        'id_municipio' => Municipio::isBauru()->select('id')->first()->id,
    ];
});
