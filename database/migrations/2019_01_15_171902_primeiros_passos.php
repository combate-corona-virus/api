<?php

use Illuminate\Database\Migrations\Migration;

class PrimeirosPassos extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        exec('php artisan passport:install');

        $database = config('database.connections.pgsql.database');
        DB::statement('ALTER DATABASE ' . $database . ' SET datestyle = "ISO, DMY";');
        DB::statement('CREATE EXTENSION IF NOT EXISTS tablefunc;');
        DB::statement('CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;');
        DB::statement('CREATE EXTENSION IF NOT EXISTS unaccent;');
        DB::statement('CREATE EXTENSION IF NOT EXISTS postgis;');
        DB::statement('CREATE EXTENSION IF NOT EXISTS postgis_topology;');
        DB::statement('CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder;');
        DB::Statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');

    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
    }
}
