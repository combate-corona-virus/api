<?php

use Illuminate\Database\Migrations\Migration;

class AddRoleAdmin extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        App\Models\Role::create([
            'name' => 'admin',
            'display_name' => 'Administrador',
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
    }
}
