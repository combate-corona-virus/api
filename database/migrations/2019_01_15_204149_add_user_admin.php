<?php

use Illuminate\Database\Migrations\Migration;

class AddUserAdmin extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        App\Models\Usuario::create([
            'nome' => 'Exemplo',
            'sobrenome' => 'Admin',
            'username' => 'exemplo-admin@combatecoronavirus.com.br',
            'email' => 'exemplo-admin@combatecoronavirus.com.br',
            'password' => 'exemplo-senha-admin', // não usar criptografia, é automático
        ])->attachRole('admin');
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {}
}
