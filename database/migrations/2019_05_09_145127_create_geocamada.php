<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Enums\NomeGeocamada;

class CreateGeocamada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geocamada', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->unsignedBigInteger('id_geocamada_pai')->nullable();
            $table->foreign('id_geocamada_pai')->references('id')->on('geocamada');
            $table->string('nome', 255)->unique();
            $table->string('nome_legivel', 255);
            $table->string('cor', 9)->default('#01B564');
            $table->string('icone', 255)->nullable();
            $table->string('icone_unicode', 255)->nullable();
            $table->boolean('is_custom')->default(false);
            $table->integer('indice')->default(0);
            $table->jsonb('query')->nullable();
            $table->softDeletes();
        });

        $pacientes = \App\Models\Geocamada::create([
                'nome' => NomeGeocamada::PACIENTES()->value,
                'nome_legivel' => 'Pacientes',
                'indice' => 0,
                'icone' => '<i class="fas fa-map-pin" aria-hidden="true"></i>',
                'icone_unicode' => 'f276'
            ]);

        //============= Agente patógeno =============
        $agentePatogeno = $pacientes->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::AGENTE_PATOGENO()->value,
            'nome_legivel' => 'Agente patógeno',
            'indice' => 0,
            'icone' => '<i class="fas fa-map-pin" aria-hidden="true"></i>',
            'icone_unicode' => 'f276'
        ]);

        $agentePatogeno->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::CONFIRMADO()->value,
            'nome_legivel' => 'Confirmado (COVID-19)',
            'indice' => 0,
            'cor' => '#dc3545', //vermelho
            'icone' => '<i class="fas fa-map-pin" aria-hidden="true"></i>',
            'icone_unicode' => 'f276'
        ]);

        $agentePatogeno->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::SUSPEITO_EM_ANALISE()->value,
            'nome_legivel' => 'Suspeito (Em análise)',
            'indice' => 1,
            'cor' => '#bdbf15', //amarelo
            'icone' => '<i class="fas fa-map-pin" aria-hidden="true"></i>',
            'icone_unicode' => 'f276'
        ]);

        $agentePatogeno->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::DESCARTADO_OUTROS()->value,
            'nome_legivel' => 'Descartado (Outros)',
            'indice' => 2,
            'cor' => '#6ef575', //verde
            'icone' => '<i class="fas fa-map-pin" aria-hidden="true"></i>',
            'icone_unicode' => 'f276'
        ]);

        //============= Condição Paciente =============
        $condicaoPaciente = $pacientes->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::CONDICAO_DO_PACIENTE()->value,
            'nome_legivel' => 'Condição do paciente',
            'indice' => 1,
            'icone' => '<i class="fas fa-map-pin" aria-hidden="true"></i>',
            'icone_unicode' => 'f276'
        ]);

        $condicaoPaciente->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::ALTA_LIBERADO_DO_ISOLAMENTO()->value,
            'nome_legivel' => 'Alta - Liberado do isolamento',
            'indice' => 0,
            'cor' => '#6ef575', //verde
            'icone' => '<i class="fas fa-map-pin" aria-hidden="true"></i>',
            'icone_unicode' => 'f276'
        ]);

        $condicaoPaciente->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::ALTA_ISOLAMENTO_SOCIAL()->value,
            'nome_legivel' => 'Alta - Isolamento Social',
            'indice' => 1,
            'cor' => '#f3f56e', //amarelo
            'icone' => '<i class="fas fa-map-pin" aria-hidden="true"></i>',
            'icone_unicode' => 'f276'
        ]);

        $condicaoPaciente->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::EM_ACOMPANHAMENTO_ESTAVEL()->value,
            'nome_legivel' => 'Em acompanhamento - Estável',
            'indice' => 2,
            'cor' => '#f5bb6e', //larajna
            'icone' => '<i class="fas fa-map-pin" aria-hidden="true"></i>',
            'icone_unicode' => 'f276'
        ]);

        $condicaoPaciente->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::EM_ACOMPANHAMENTO_GRAVE()->value,
            'nome_legivel' => 'Em acompanhamento - Grave',
            'indice' => 3,
            'cor' => '#dc3545', //vermelho
            'icone' => '<i class="fas fa-map-pin" aria-hidden="true"></i>',
            'icone_unicode' => 'f276'
        ]);

        $condicaoPaciente->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::OBITO()->value,
            'nome_legivel' => 'Óbito',
            'indice' => 4,
            'cor' => '#000000', //preto
            'icone' => '<i class="fas fa-map-pin" aria-hidden="true"></i>',
            'icone_unicode' => 'f276'
        ]);

        //============= Incidência de Suspeitas =============
        $incidenciaSuspeitas = $pacientes->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::INCIDENCIA_DE_SUSPEITAS()->value,
            'nome_legivel' => 'Incidência de suspeitas',
            'indice' => 2,
            'icone' => '<i class="fas fa-map-pin" aria-hidden="true"></i>',
        ]);

        $incidenciaSuspeitas->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::MAPA_CALOR_INCIDENCIA_DE_SUSPEITAS()->value,
            'nome_legivel' => 'Suspeitas (Mapa de Calor)',
            'indice' => 0,
            'cor' => '#dc3545', //vermelho
            'icone' => '<i class="fa fa-map" aria-hidden="true"></i>',
        ]);

        //============= Incidência de Confirmados =============
        $incidenciaConfirmados = $pacientes->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::INCIDENCIA_DE_CONFIRMADOS()->value,
            'nome_legivel' => 'Incidência de confirmados',
            'indice' => 2,
            'icone' => '<i class="fas fa-map-pin" aria-hidden="true"></i>',
        ]);

        $incidenciaConfirmados->geocamadasFilhas()->create([
            'nome' => NomeGeocamada::MAPA_CALOR_INCIDENCIA_DE_CONFIRMADOS()->value,
            'nome_legivel' => 'Confirmados (Mapa de Calor)',
            'indice' => 0,
            'cor' => '#dc3545', //vermelho
            'icone' => '<i class="fa fa-map" aria-hidden="true"></i>',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geocamada');
    }
}
