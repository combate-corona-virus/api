<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Municipio;
use App\Models\Usuario;

class AlterUsuarioAddMunicipioRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usuario', function (Blueprint $table) {
            $table->unsignedBigInteger('id_municipio')->nullable();
            $table->foreign('id_municipio')->references('id')->on('municipio')->nullable();
        });

        $bauru = Municipio::isBauru()->first();
        Usuario::withTrashed()->update(['id_municipio' => $bauru->id]);

        Schema::table('usuario', function (Blueprint $table) {
            $table->unsignedBigInteger('id_municipio')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE usuario DROP COLUMN IF EXISTS id_municipio");
    }
}
