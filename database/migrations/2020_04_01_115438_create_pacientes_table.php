<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use MStaack\LaravelPostgis\Schema\Blueprint;
use App\Enums\PacienteTipo;
use App\Enums\PacienteCondicao;
use App\Enums\PacienteFaixaEtaria;
use App\Enums\PacienteStatusExame;
use App\Enums\PacienteAgentePatogeno;

use Facades\App\Helpers\EnumHelper;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EnumHelper::create('paciente_tipo', PacienteTipo::getValues());
        EnumHelper::create('paciente_faixa_etaria', PacienteFaixaEtaria::getValues());
        EnumHelper::create('paciente_condicao', PacienteCondicao::getValues());
        EnumHelper::create('paciente_status_exame', PacienteStatusExame::getValues());
        EnumHelper::create('paciente_agente_patogeno', PacienteAgentePatogeno::getValues());

        Schema::create('paciente', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_municipio');
            $table->foreign('id_municipio')->references('id')->on('municipio');
            $table->integer('numero_notificacao')->unique();
            $table->string('endereco')->nullable();
            $table->string('residencia')->nullable();
            $table->dateTime('data_notificacao')->nullable();
            $table->char('sexo', 1)->nullable();
            $table->smallInteger('idade')->nullable();
            $table->dateTime('data_alta_obito')->nullable();
            $table->dateTime('data_coleta')->nullable();
            $table->point('geom', 'GEOMETRY', 4326)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        DB::Statement("ALTER TABLE paciente
            ADD COLUMN tipo paciente_tipo,
            ADD COLUMN faixa_etaria paciente_faixa_etaria,
            ADD COLUMN condicao paciente_condicao,
            ADD COLUMN status_exame paciente_status_exame,
            ADD COLUMN agente_patogeno paciente_agente_patogeno;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paciente');
        EnumHelper::drop('paciente_tipo');
        EnumHelper::drop('paciente_faixa_etaria');
        EnumHelper::drop('paciente_condicao');
        EnumHelper::drop('paciente_status_exame');
        EnumHelper::drop('paciente_agente_patogeno');
    }
}
