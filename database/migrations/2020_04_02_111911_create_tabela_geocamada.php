<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\PolymorphicType;

class CreateTabelaGeocamada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabela_geocamada', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('id_geocamada');
            $table->foreign('id_geocamada')->references('id')->on('geocamada');

            $table->morphs(PolymorphicType::POLYMORPHIC_FIELD_NAME()->value);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabela_geocamada');
    }
}
