<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Enums\TipoCaso;
use Facades\App\Helpers\EnumHelper;

class CreateBoletimEpidemiologicoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletim_epidemiologico', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_municipio');
            $table->dateTime('data');
            $table->integer('quantidade_suspeitos');
            $table->integer('quantidade_confirmados');
            $table->integer('quantidade_obitos');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_municipio')->references('id')->on('municipio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletim_epidemiologico');
    }
}
