<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnidadeFederativaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidade_federativa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('regiao');
            $table->string('nome');
            $table->integer('geocodigo');
            $table->multiPolygon('geom')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('municipio', function (Blueprint $table) {
            $table->unsignedBigInteger('id_unidade_federativa')->nullable();
            $table->foreign('id_unidade_federativa')->references('id')->on('unidade_federativa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('municipio', function(Blueprint $table) {
            $table->dropForeign('municipio_id_unidade_federativa_foreign');
            $table->dropColumn('id_unidade_federativa');
        });
        Schema::dropIfExists('unidade_federativa');
    }
}
