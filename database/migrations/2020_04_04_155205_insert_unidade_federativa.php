<?php

use Illuminate\Database\Migrations\Migration;
use App\Imports\ImportUnidadeFederativa;
use Maatwebsite\Excel\Facades\Excel;

class InsertUnidadeFederativa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $path = storage_path('importacao/uf.csv');

        Excel::import(new ImportUnidadeFederativa(), $path);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
