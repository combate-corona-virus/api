<?php

use Illuminate\Database\Migrations\Migration;
use App\Imports\ImportMunicipio;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Municipio;
use App\Models\Usuario;

class InsertMunicipio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usuario', function (Blueprint $table) {
            $table->unsignedBigInteger('id_municipio')->nullable()->change();
        });

        Usuario::withTrashed()->update(['id_municipio' => null]);
        Municipio::withTrashed()->whereNotNull('id')->forceDelete();

        $path = storage_path('importacao/municipio.csv');
        Excel::import(new ImportMunicipio(), $path);

        $bauru = Municipio::isBauru()->first();
        $bauru->update(['is_participante' => true]);

        Usuario::withTrashed()->update(['id_municipio' => $bauru->id]);

        Schema::table('usuario', function (Blueprint $table) {
            $table->unsignedBigInteger('id_municipio')->nullable(false)->change();
        });

        Schema::table('municipio', function (Blueprint $table) {
            $table->unsignedBigInteger('id_unidade_federativa')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
