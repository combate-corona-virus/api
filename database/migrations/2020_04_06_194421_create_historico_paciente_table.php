<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricoPacienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico_paciente', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_municipio');
            $table->foreign('id_municipio')->references('id')->on('municipio');
            $table->string('endereco')->nullable();
            $table->string('residencia')->nullable();
            $table->dateTime('data_notificacao')->nullable();
            $table->char('sexo', 1)->nullable();
            $table->smallInteger('idade')->nullable();
            $table->dateTime('data_alta_obito')->nullable();
            $table->dateTime('data_coleta')->nullable();
            $table->point('geom', 'GEOMETRY', 4326)->nullable();

            $table->timestamps();

            $table->unsignedBigInteger('id_paciente');
            $table->foreign('id_paciente')->references('id')->on('paciente');
        });

        \DB::Statement("ALTER TABLE historico_paciente
            ADD COLUMN tipo paciente_tipo,
            ADD COLUMN faixa_etaria paciente_faixa_etaria,
            ADD COLUMN condicao paciente_condicao,
            ADD COLUMN status_exame paciente_status_exame,
            ADD COLUMN agente_patogeno paciente_agente_patogeno,
            ADD UNIQUE (id_paciente, data_notificacao);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historico_paciente');
    }
}
