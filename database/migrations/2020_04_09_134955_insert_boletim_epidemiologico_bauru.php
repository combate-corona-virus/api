<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\BoletimEpidemiologico;
use App\Models\Municipio;

class InsertBoletimEpidemiologicoBauru extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $bauru = Municipio::isBauru()->first();

        foreach ($this->dados as $dados) {
            $boletim = BoletimEpidemiologico::where('id_municipio', $bauru->id)
                ->where('data', $dados['data'])->first() ?? new BoletimEpidemiologico();

            $dados['id_municipio'] = $bauru->id;
            $boletim->fill($dados);
            $boletim->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    //data;quantidade_suspeitos;quantidade_confirmados;quatidade_obitos

    private $dados = [
        ["data" => "2020-03-16", "quantidade_suspeitos" => 11, 'quantidade_confirmados' => 0, 'quantidade_obitos' => 0],
        ["data" => "2020-03-17", "quantidade_suspeitos" => 19, 'quantidade_confirmados' => 0, 'quantidade_obitos' => 0],
        ["data" => "2020-03-18", "quantidade_suspeitos" => 26, 'quantidade_confirmados' => 0, 'quantidade_obitos' => 0],
        ["data" => "2020-03-19", "quantidade_suspeitos" => 29, 'quantidade_confirmados' => 0, 'quantidade_obitos' => 0],
        ["data" => "2020-03-20", "quantidade_suspeitos" => 36, 'quantidade_confirmados' => 0, 'quantidade_obitos' => 0],
        ["data" => "2020-03-22", "quantidade_suspeitos" => 49, 'quantidade_confirmados' => 0, 'quantidade_obitos' => 0],
        ["data" => "2020-03-23", "quantidade_suspeitos" => 57, 'quantidade_confirmados' => 0, 'quantidade_obitos' => 0],
        ["data" => "2020-03-24", "quantidade_suspeitos" => 60, 'quantidade_confirmados' => 0, 'quantidade_obitos' => 0],
        ["data" => "2020-03-25", "quantidade_suspeitos" => 71, 'quantidade_confirmados' => 0, 'quantidade_obitos' => 0],
        ["data" => "2020-03-26", "quantidade_suspeitos" => 86, 'quantidade_confirmados' => 0, 'quantidade_obitos' => 0],
        ["data" => "2020-03-27", "quantidade_suspeitos" => 104, 'quantidade_confirmados' => 0, 'quantidade_obitos' =>  0],
        ["data" => "2020-03-28", "quantidade_suspeitos" => 107, 'quantidade_confirmados' => 0, 'quantidade_obitos' =>  0],
        ["data" => "2020-03-29", "quantidade_suspeitos" => 108, 'quantidade_confirmados' => 0, 'quantidade_obitos' =>  0],
        ["data" => "2020-03-30", "quantidade_suspeitos" => 130, 'quantidade_confirmados' => 2, 'quantidade_obitos' =>  0],
        ["data" => "2020-03-31", "quantidade_suspeitos" => 132, 'quantidade_confirmados' => 2, 'quantidade_obitos' =>  0],
        ["data" => "2020-04-01", "quantidade_suspeitos" => 167, 'quantidade_confirmados' => 3, 'quantidade_obitos' =>  0],
        ["data" => "2020-04-02", "quantidade_suspeitos" => 174, 'quantidade_confirmados' => 3, 'quantidade_obitos' =>  0],
        ["data" => "2020-04-03", "quantidade_suspeitos" => 181, 'quantidade_confirmados' => 3, 'quantidade_obitos' =>  0],
        ["data" => "2020-04-04", "quantidade_suspeitos" => 183, 'quantidade_confirmados' => 4, 'quantidade_obitos' =>  0],
        ["data" => "2020-04-05", "quantidade_suspeitos" => 178, 'quantidade_confirmados' => 6, 'quantidade_obitos' =>  1],
        ["data" => "2020-04-06", "quantidade_suspeitos" => 201, 'quantidade_confirmados' => 6, 'quantidade_obitos' =>  1],
        ["data" => "2020-04-07", "quantidade_suspeitos" => 219, 'quantidade_confirmados' => 7, 'quantidade_obitos' =>  1],
        ["data" => "2020-04-08", "quantidade_suspeitos" => 227, 'quantidade_confirmados' => 9, 'quantidade_obitos' =>  1],
    ];
}
