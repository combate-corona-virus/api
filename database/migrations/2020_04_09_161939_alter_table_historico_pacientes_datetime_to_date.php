<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableHistoricoPacientesDatetimeToDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::Statement("ALTER TABLE historico_paciente ALTER COLUMN data_alta_obito TYPE date USING data_alta_obito::DATE;");
        \DB::Statement("ALTER TABLE historico_paciente ALTER COLUMN data_coleta TYPE date USING data_coleta::DATE;");
        \DB::Statement("ALTER TABLE historico_paciente ALTER COLUMN data_notificacao TYPE date USING data_notificacao::DATE;");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::Statement("ALTER TABLE historico_paciente ALTER COLUMN data_alta_obito TYPE timestamp USING data_alta_obito::TIMESTAMP;");
        \DB::Statement("ALTER TABLE historico_paciente ALTER COLUMN data_coleta TYPE timestamp USING data_coleta::TIMESTAMP;");
        \DB::Statement("ALTER TABLE historico_paciente ALTER COLUMN data_notificacao TYPE timestamp USING data_notificacao::TIMESTAMP;");
    }
}
