<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMunicipioUnidadeFederativaAlterGeom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::Statement("ALTER TABLE municipio
            ALTER COLUMN geom TYPE geometry(MultiPolygon,4326) USING geom::geometry(MultiPolygon,4326);");

        \DB::Statement("ALTER TABLE unidade_federativa
            ALTER COLUMN geom TYPE geometry(MultiPolygon,4326) USING geom::geometry(MultiPolygon,4326);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::Statement("ALTER TABLE municipio
            ALTER COLUMN geom TYPE geography(MultiPolygon,4326) USING geom::geography(MultiPolygon,4326);");

        \DB::Statement("ALTER TABLE unidade_federativa
            ALTER COLUMN geom TYPE geography(MultiPolygon,4326) USING geom::geography(MultiPolygon,4326);");
    }
}
