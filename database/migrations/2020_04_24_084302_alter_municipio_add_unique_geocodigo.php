<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMunicipioAddUniqueGeocodigo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::Statement('ALTER TABLE municipio ADD CONSTRAINT municipio_geocodigo_unique UNIQUE(geocodigo)');
        DB::Statement('ALTER TABLE municipio ALTER COLUMN geocodigo SET NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::Statement('ALTER TABLE municipio DROP CONSTRAINT municipio_geocodigo_unique');
        DB::Statement('ALTER TABLE municipio ALTER COLUMN geocodigo DROP NOT NULL');
    }
}
