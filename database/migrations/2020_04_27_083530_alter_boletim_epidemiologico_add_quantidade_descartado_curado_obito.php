<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBoletimEpidemiologicoAddQuantidadeDescartadoCuradoObito extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('boletim_epidemiologico', function (Blueprint $table) {
            $table->integer('quantidade_descartados')->nullable();
            $table->integer('quantidade_curados')->nullable();
            $table->integer('quantidade_obitos_suspeitos')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE boletim_epidemiologico DROP COLUMN IF EXISTS quantidade_descartados");
        DB::statement("ALTER TABLE boletim_epidemiologico DROP COLUMN IF EXISTS quantidade_curados");
        DB::statement("ALTER TABLE boletim_epidemiologico DROP COLUMN IF EXISTS quantidade_obitos_suspeitos");
    }
}
