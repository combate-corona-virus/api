<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMunicipioTableAddInformacoesMunicipio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('municipio', function (Blueprint $table) {
            $table->unsignedBigInteger('id_arquivo_logo')->nullable();
            $table->text('texto_contato')->nullable();
            $table->foreign('id_arquivo_logo')->references('id')->on('arquivo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE municipio DROP COLUMN IF EXISTS id_arquivo_logo");
    }
}
