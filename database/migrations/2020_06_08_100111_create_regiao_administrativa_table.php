<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegiaoAdministrativaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regiao_administrativa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome', 50)->nullable();
            $table->timestamps();
        });

        \DB::table('regiao_administrativa')->insert([
            ['nome' => 'Região administrativa de Bauru'],
        ]);

        \DB::Statement("ALTER TABLE municipio ADD COLUMN id_regiao_administrativa INTEGER REFERENCES regiao_administrativa(id);");

        \DB::Statement("UPDATE municipio SET id_regiao_administrativa = 
        (SELECT id FROM regiao_administrativa WHERE nome LIKE 'Região administrativa de Bauru') 
        WHERE UPPER(nome) IN 
        (
         'AGUDOS',
         'AREALVA',
         'AVAÍ',
         'BALBINOS',
         'BARIRI',
         'BARRA BONITA',
         'BAURU',
         'BOCAINA',
         'BORACÉIA',
         'BOREBI',
         'ITAJU',
         'ITAPUÍ',
         'JAÚ',
         'LENÇÓIS PAULISTA',
         'LINS',
         'LUCIANÓPOLIS',
         'MACATUBA',
         'MINEIROS DO TIETÊ',
         'PAULISTÂNIA',
         'PEDERNEIRAS',
         'PIRAJUÍ',
         'PIRATININGA',
         'PONGAÍ',
         'PRESIDENTE ALVES',
         'PROMISSÃO',
         'REGINÓPOLIS',
         'SABINO',
         'UBIRAJARA',
         'URU',
         'CABRÁLIA PAULISTA',
         'CAFELÂNDIA',
         'DOIS CÓRREGOS',
         'DUARTINA',
         'GETULINA',
         'GUAIÇARA',
         'GUAIMBÊ',
         'GUARANTÃ',
         'IACANGA',
         'IGARAÇU DO TIETÊ'
        ) AND geocodigo::text LIKE '35%';");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::Statement("ALTER TABLE municipio DROP COLUMN id_regiao_administrativa;");
        Schema::dropIfExists('regiao_administrativa');
    }
}
