<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableBoletimEpidemiologico extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        // CRIA TABELAS DE REFERÊNCIA:

        // CURVA EPIDEMIOLÓGICA

        \DB::Statement('CREATE TABLE referencia_curva_epidemiologica (
				id SERIAL PRIMARY KEY,
				minimo NUMERIC(5,2),
				maximo NUMERIC(5,2),
				valor_atribuido NUMERIC(2,1)
				);');

        \DB::Statement('INSERT INTO referencia_curva_epidemiologica (minimo, maximo, valor_atribuido) VALUES
				(0.0, 50.00, 1),
				(50.01, 54.44, 0.9),
				(54.45, 58.89, 0.8),
				(58.90, 63.33, 0.7),
				(63.34, 67.78, 0.6),
				(67.79, 72.22, 0.5),
				(72.23, 76.67, 0.4),
				(76.68, 81.11, 0.3),
				(81.12, 85.56, 0.2),
				(85.57, 90.00, 0.1),
				(90.01, 100.00, 0);');

        // OBITOS

        \DB::Statement('CREATE TABLE referencia_obitos (
				id SERIAL PRIMARY KEY,
				minimo NUMERIC(5,2),
				maximo NUMERIC(5,2),
				valor_atribuido NUMERIC(2,1)
				);');

        \DB::Statement('INSERT INTO referencia_obitos (minimo, maximo, valor_atribuido) VALUES
				(0.0, 5.0, 1.0),
				(5.01, 5.56, 0.9),
				(5.57, 6.11, 0.8),
				(6.12, 6.67, 0.7),
				(6.68, 7.22, 0.6),
				(7.23, 7.78, 0.5),
				(7.79, 8.33, 0.4),
				(8.34, 8.89, 0.3),
				(8.90, 9.44, 0.2),
				(9.45, 10.00, 0.1),
				(10.01, 100.00, 0.0);');

        // TESTAGEM

        \DB::Statement('CREATE TABLE referencia_testagem (
				id SERIAL PRIMARY KEY,
				minimo INTEGER,
				maximo INTEGER,
				valor_atribuido NUMERIC(2,1)
				);');

        \DB::Statement('INSERT INTO referencia_testagem (minimo, maximo, valor_atribuido) VALUES
				(0, 7520, 0.0),
				(7521, 15040, 0.1),
				(15041, 22560, 0.2),
				(22561, 30080, 0.3),
				(30081, 37600, 0.4),
				(37601, 45120, 0.5),
				(45121, 52640, 0.6),
				(52641, 60160, 0.7),
				(60161, 67680, 0.8),
				(67681, 75200, 0.9),
				(75201, 100000000, 1);');

        // LEITOS

        \DB::Statement('CREATE TABLE referencia_leitos (
				id SERIAL PRIMARY KEY,
				minimo NUMERIC(5,2),
				maximo NUMERIC(5,2),
				valor_atribuido NUMERIC(2,1)
				);');

        \DB::Statement('INSERT INTO referencia_leitos (minimo, maximo, valor_atribuido) VALUES
				(0.00, 112.80, 0),
				(112.81, 137.87, 0.1),
				(137.88, 162.93, 0.2),
				(162.94, 188.00, 0.3),
				(188.01, 213.07, 0.4),
				(213.08, 238.13, 0.5),
				(238.14, 263.20, 0.6),
				(263.21, 288.27, 0.7),
				(288.28, 313.33, 0.8),
				(313.34, 338.40, 0.9),
				(338.41, 999.99, 1);');

        // TAXA DE OCUPAÇÃO

        \DB::Statement('CREATE TABLE referencia_taxa_ocupacao (
				id SERIAL PRIMARY KEY,
				minimo NUMERIC(5,2),
				maximo NUMERIC(5,2),
				valor_atribuido NUMERIC(2,1)
				);');

        \DB::Statement('INSERT INTO referencia_taxa_ocupacao (minimo, maximo, valor_atribuido) VALUES
				(0.00, 60.00, 1),
				(60.01, 63.89, 0.9),
				(63.90, 67.78, 0.8),
				(67.79, 71.67, 0.7),
				(71.68, 75.56, 0.6),
				(75.57, 79.44, 0.5),
				(79.45, 83.33, 0.4),
				(83.34, 87.22, 0.3),
				(87.23, 91.11, 0.2),
				(91.12, 95.00, 0.1),
				(95.01, 100.00, 0);');

        // ISOLAMENTO SOCIAL

        \DB::Statement('CREATE TABLE referencia_isolamento_social (
				id SERIAL PRIMARY KEY,
				minimo NUMERIC(5,2),
				maximo NUMERIC(5,2),
				valor_atribuido NUMERIC(2,1)
			);');

        \DB::Statement('INSERT INTO referencia_isolamento_social (minimo, maximo, valor_atribuido) VALUES
				(0.00, 30.00, 0),
				(30.01, 32.78, 0.1),
				(32.79, 35.56, 0.2),
				(35.57, 38.33, 0.3),
				(38.34, 41.11, 0.4),
				(41.12, 43.89, 0.5),
				(43.90, 46.67, 0.6),
				(46.68, 49.44, 0.7),
				(49.45, 52.22, 0.8),
				(52.23, 55.00, 0.9),
				(55.01, 100.00, 1);');

        // TESTAGEM (REGIONAL)

        \DB::Statement('CREATE TABLE referencia_testagem_regional (
				id SERIAL PRIMARY KEY,
				minimo INTEGER,
				maximo INTEGER,
				valor_atribuido NUMERIC(2,1)
				);');

        \DB::Statement('INSERT INTO referencia_testagem_regional (minimo, maximo, valor_atribuido) VALUES
				(0, 23124, 0.0),
				(23125, 46247, 0.1),
				(46248, 69371, 0.2),
				(69372, 92494, 0.3),
				(92495, 115618, 0.4),
				(115619, 138741, 0.5),
				(138742, 161865, 0.6),
				(161866, 184988, 0.7),
				(184989, 208112, 0.8),
				(208113, 231235, 0.9),
				(231235, 100000000, 1);');

        // LEITOS (REGIONAL)

        \DB::Statement('CREATE TABLE referencia_leitos_regional (
				id SERIAL PRIMARY KEY,
				minimo NUMERIC(5,2),
				maximo NUMERIC(5,2),
				valor_atribuido NUMERIC(2,1)
				);');

        \DB::Statement('INSERT INTO referencia_leitos_regional (minimo, maximo, valor_atribuido) VALUES
				(0.00, 116.00, 0),
				(116.01, 128.78, 0.1),
				(128.79, 141.56, 0.2),
				(141.57, 154.33, 0.3),
				(154.34, 167.11, 0.4),
				(167.12, 179.89, 0.5),
				(179.90, 192.67, 0.6),
				(192.68, 205.44, 0.7),
				(205.45, 218.22, 0.8),
				(218.23, 231.00, 0.9),
				(231.01, 999.99, 1);');

        // SITUACAO

        \DB::Statement('CREATE TABLE referencia_situacao (
				id SERIAL PRIMARY KEY,
				minimo NUMERIC(5,4),
				maximo NUMERIC(5,4),
				situacao VARCHAR(40)
				);');

        \DB::Statement("INSERT INTO referencia_situacao (minimo, maximo, situacao) VALUES
				(0.00, 0.2499, '1 - LOCKDOWN'),
				(0.2500, 0.4999, '2 - SOMENTE ESSENCIAL'),
				(0.5000, 0.7499, '3 - ABERTURA PARCIAL'),
				(0.7500, 0.9999, '4 - ABERTURA QUASE TOTAL'),
				(1.00, 9.9999, '5 - VIDA NORMAL');");

        // ADICIONA CAMPOS A BOLETIM EPIDEMIOLÓGICO

        \DB::Statement('ALTER TABLE boletim_epidemiologico 
                        ADD COLUMN quantidade_confirmados_previsto_fiocruz INTEGER,
                        ADD COLUMN quantidade_testagem INTEGER,
                        ADD COLUMN quantidade_leitos_uti INTEGER,
                        ADD COLUMN quantidade_leitos_uti_ocupados INTEGER,
                        ADD COLUMN indice_isolamento_social NUMERIC(5,2);');
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        \DB::Statement('DROP TABLE referencia_testagem_regional;');
        \DB::Statement('DROP TABLE referencia_leitos_regional;');
        \DB::Statement('DROP TABLE referencia_situacao;');
        \DB::Statement('DROP TABLE referencia_curva_epidemiologica;');
        \DB::Statement('DROP TABLE referencia_obitos;');
        \DB::Statement('DROP TABLE referencia_testagem;');
        \DB::Statement('DROP TABLE referencia_leitos;');
        \DB::Statement('DROP TABLE referencia_taxa_ocupacao;');
        \DB::Statement('DROP TABLE referencia_isolamento_social;');

        \DB::Statement('ALTER TABLE boletim_epidemiologico 
                        DROP COLUMN quantidade_confirmados_previsto_fiocruz,
                        DROP COLUMN quantidade_testagem,
                        DROP COLUMN quantidade_leitos_uti,
                        DROP COLUMN quantidade_leitos_uti_ocupados,
                        DROP COLUMN indice_isolamento_social;');
    }
}
