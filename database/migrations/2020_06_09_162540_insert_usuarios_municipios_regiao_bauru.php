<?php

use Illuminate\Database\Migrations\Migration;
use App\Models\Municipio;
use App\Models\Usuario;

class InsertUsuariosMunicipiosRegiaoBauru extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $relacaoMuinicipioEmails = $this->getRelacaoMuinicipioEmails();
        foreach ($relacaoMuinicipioEmails as $municipioEmail) {
            $municipio = Municipio::where('geocodigo', $municipioEmail['geocodigo'])->first();
            if ($municipio) {
                Usuario::create([
                    'nome' => $municipio->nome_prefeitura,
                    'email' => $municipioEmail['email'],
                    'password' => $municipioEmail['password'],
                    'id_municipio' => $municipio->id,
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $relacaoMuinicipioEmails = $this->getRelacaoMuinicipioEmails();
        foreach ($relacaoMuinicipioEmails as $municipioEmail) {
            $municipio = Municipio::where('geocodigo', $municipioEmail['geocodigo'])->first();
            if ($municipio) {
                $municipio->usuarios()->where('email', $municipioEmail['email'])->forceDelete();
            }
        }
    }

    public function getRelacaoMuinicipioEmails()
    {
        return [
            ['geocodigo' => '3506003', 'email' => 'prefeitura@exemplo.sp.gov.br', 'password' =>  'exemplo-senha-prefeitura'],
        ];
    }
}
