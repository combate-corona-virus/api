<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEstruturaIprAtual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // CRIA NOVAS TABELAS DE REFERÊNCIA:
		// CURVA EPIDEMIOLÓGICA
        \DB::Statement('DROP TABLE referencia_curva_epidemiologica;');
        \DB::Statement('CREATE TABLE referencia_curva_epidemiologica (
            id SERIAL PRIMARY KEY,
            minimo NUMERIC(7,2),
            maximo NUMERIC(7,2),
            valor_atribuido NUMERIC(3,2)
            );');
        \DB::Statement('INSERT INTO referencia_curva_epidemiologica (minimo, maximo, valor_atribuido) VALUES
        (0.0, 90.00, 1),
        (90.01, 92.22, 0.9),
        (92.23, 94.44, 0.8),
        (94.45, 96.67, 0.7),
        (96.68, 98.89, 0.6),
        (98.90, 101.11, 0.5),
        (101.12, 103.33, 0.4),
        (103.34, 105.56, 0.3),
        (105.57, 107.78, 0.2),
        (107.79, 110.00, 0.1),
        (110.01, 99999.99, 0.05);');

        // TESTAGEM
        \DB::Statement('ALTER TABLE referencia_testagem ALTER COLUMN valor_atribuido TYPE NUMERIC(3,2) USING valor_atribuido::NUMERIC(3,2);');
        \DB::Statement('UPDATE referencia_testagem SET valor_atribuido = 0.05 WHERE minimo = 0 AND maximo = 7520;');

        // LEITOS
        \DB::Statement('DROP TABLE referencia_leitos;');
        \DB::Statement('CREATE TABLE referencia_leitos (
            id SERIAL PRIMARY KEY,
            minimo NUMERIC(7,2),
            maximo NUMERIC(7,2),
            valor_atribuido NUMERIC(3,2)
            );');
        \DB::Statement('INSERT INTO referencia_leitos (minimo, maximo, valor_atribuido) VALUES
        (0.00, 363.05, 0.05),
        (363.06, 443.72, 0.1),
        (443.73, 524.40, 0.2),
        (524.41, 605.08, 0.3),
        (605.09, 685.75, 0.4),
        (685.76, 776.43, 0.5),
        (776.44, 847.11, 0.6),
        (847.12, 927.78, 0.7),
        (927.79, 1008.46, 0.8),
        (1008.47, 1089.14, 0.9),
        (1089.15, 99999.99, 1);');

        // TAXA DE OCUPAÇÃO
        \DB::Statement('DROP TABLE referencia_taxa_ocupacao;');
        \DB::Statement('CREATE TABLE referencia_taxa_ocupacao (
            id SERIAL PRIMARY KEY,
            minimo NUMERIC(5,2),
            maximo NUMERIC(5,2),
            valor_atribuido NUMERIC(2,1)
            );');

        \DB::Statement('INSERT INTO referencia_taxa_ocupacao (minimo, maximo, valor_atribuido) VALUES
        (0.00, 50.00, 1),
        (50.01, 53.89, 0.9),
        (53.90, 57.78, 0.8),
        (57.79, 61.67, 0.7),
        (61.68, 65.56, 0.6),
        (65.57, 69.44, 0.5),
        (69.45, 73.33, 0.4),
        (73.34, 77.22, 0.3),
        (77.23, 81.11, 0.2),
        (81.12, 85.00, 0.1),
        (85.01, 999.99, 0);');

        // ISOLAMENTO SOCIAL
        \DB::Statement('DROP TABLE referencia_isolamento_social;');
        \DB::Statement('CREATE TABLE referencia_isolamento_social (
            id SERIAL PRIMARY KEY,
            minimo NUMERIC(5,2),
            maximo NUMERIC(5,2),
            valor_atribuido NUMERIC(2,1)
            );');
        \DB::Statement('INSERT INTO referencia_isolamento_social (minimo, maximo, valor_atribuido) VALUES
        (0.00, 35.00, 0),
        (35.01, 38.89, 0.1),
        (38.90, 42.78, 0.2),
        (42.79, 46.67, 0.3),
        (46.68, 50.56, 0.4),
        (50.57, 54.44, 0.5),
        (54.45, 58.33, 0.6),
        (58.34, 62.22, 0.7),
        (62.23, 66.11, 0.8),
        (66.12, 70.00, 0.9),
        (70.01, 100.00, 1);');

        // SITUACAO
        \DB::Statement('DROP TABLE referencia_situacao;');
        \DB::Statement('CREATE TABLE referencia_situacao (
            id SERIAL PRIMARY KEY,
            minimo NUMERIC(5,4),
            maximo NUMERIC(5,4),
            situacao VARCHAR(40)
            );');
        \DB::Statement("INSERT INTO referencia_situacao (minimo, maximo, situacao) VALUES
        (0.00, 0.2499, '1 - SOMENTE ESSENCIAL'),
        (0.2500, 0.4999, '2 - ABERTURA PARCIAL I'),
        (0.5000, 0.7499, '3 - ABERTURA PARCIAL II'),
        (0.7500, 0.9999, '4 - ABERTURA PARCIAL III'),
        (1.00, 9.9999, '5 - RETOMADA NORMALIDADE');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // VOLTA AS TABELAS DE REFERÊNCIA ORIGINAIS:
        // CURVA EPIDEMIOLÓGICA
        \DB::Statement('DROP TABLE referencia_curva_epidemiologica;');
        \DB::Statement('CREATE TABLE referencia_curva_epidemiologica (
            id SERIAL PRIMARY KEY,
            minimo NUMERIC(5,2),
            maximo NUMERIC(5,2),
            valor_atribuido NUMERIC(2,1)
            );');
        \DB::Statement('INSERT INTO referencia_curva_epidemiologica (minimo, maximo, valor_atribuido) VALUES
        (0.0, 50.00, 1),
        (50.01, 54.44, 0.9),
        (54.45, 58.89, 0.8),
        (58.90, 63.33, 0.7),
        (63.34, 67.78, 0.6),
        (67.79, 72.22, 0.5),
        (72.23, 76.67, 0.4),
        (76.68, 81.11, 0.3),
        (81.12, 85.56, 0.2),
        (85.57, 90.00, 0.1),
        (90.01, 100.00, 0);');

        // TESTAGEM
        \DB::Statement('UPDATE referencia_testagem SET valor_atribuido = 0.0 WHERE minimo = 0 AND maximo = 7520;');
        \DB::Statement('ALTER TABLE referencia_testagem ALTER COLUMN valor_atribuido TYPE NUMERIC(2,1) USING valor_atribuido::NUMERIC(2,1);');

        // LEITOS
        \DB::Statement('DROP TABLE referencia_leitos;');
        \DB::Statement('CREATE TABLE referencia_leitos (
            id SERIAL PRIMARY KEY,
            minimo NUMERIC(5,2),
            maximo NUMERIC(5,2),
            valor_atribuido NUMERIC(2,1)
            );');
        \DB::Statement('INSERT INTO referencia_leitos (minimo, maximo, valor_atribuido) VALUES
        (0.00, 112.80, 0),
        (112.81, 137.87, 0.1),
        (137.88, 162.93, 0.2),
        (162.94, 188.00, 0.3),
        (188.01, 213.07, 0.4),
        (213.08, 238.13, 0.5),
        (238.14, 263.20, 0.6),
        (263.21, 288.27, 0.7),
        (288.28, 313.33, 0.8),
        (313.34, 338.40, 0.9),
        (338.41, 999.99, 1);');

        // TAXA DE OCUPAÇÃO
        \DB::Statement('DROP TABLE referencia_taxa_ocupacao;');
        \DB::Statement('CREATE TABLE referencia_taxa_ocupacao (
            id SERIAL PRIMARY KEY,
            minimo NUMERIC(5,2),
            maximo NUMERIC(5,2),
            valor_atribuido NUMERIC(2,1)
            );');
        \DB::Statement('INSERT INTO referencia_taxa_ocupacao (minimo, maximo, valor_atribuido) VALUES
        (0.00, 60.00, 1),
        (60.01, 63.89, 0.9),
        (63.90, 67.78, 0.8),
        (67.79, 71.67, 0.7),
        (71.68, 75.56, 0.6),
        (75.57, 79.44, 0.5),
        (79.45, 83.33, 0.4),
        (83.34, 87.22, 0.3),
        (87.23, 91.11, 0.2),
        (91.12, 95.00, 0.1),
        (95.01, 100.00, 0);');

        // ISOLAMENTO SOCIAL
        \DB::Statement('DROP TABLE referencia_isolamento_social;');
        \DB::Statement('CREATE TABLE referencia_isolamento_social (
            id SERIAL PRIMARY KEY,
            minimo NUMERIC(5,2),
            maximo NUMERIC(5,2),
            valor_atribuido NUMERIC(2,1)
            );');
        \DB::Statement('INSERT INTO referencia_isolamento_social (minimo, maximo, valor_atribuido) VALUES
        (0.00, 30.00, 0),
        (30.01, 32.78, 0.1),
        (32.79, 35.56, 0.2),
        (35.57, 38.33, 0.3),
        (38.34, 41.11, 0.4),
        (41.12, 43.89, 0.5),
        (43.90, 46.67, 0.6),
        (46.68, 49.44, 0.7),
        (49.45, 52.22, 0.8),
        (52.23, 55.00, 0.9),
        (55.01, 100.00, 1);');

        // SITUACAO
        \DB::Statement('DROP TABLE referencia_situacao;');
        \DB::Statement('CREATE TABLE referencia_situacao (
            id SERIAL PRIMARY KEY,
            minimo NUMERIC(5,4),
            maximo NUMERIC(5,4),
            situacao VARCHAR(40)
            );');
        \DB::Statement("INSERT INTO referencia_situacao (minimo, maximo, situacao) VALUES
        (0.00, 0.2499, '1 - LOCKDOWN'),
        (0.2500, 0.4999, '2 - SOMENTE ESSENCIAL'),
        (0.5000, 0.7499, '3 - ABERTURA PARCIAL'),
        (0.7500, 0.9999, '4 - ABERTURA QUASE TOTAL'),
        (1.00, 9.9999, '5 - VIDA NORMAL');");
    }
}
