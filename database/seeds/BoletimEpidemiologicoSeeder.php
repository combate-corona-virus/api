<?php

use App\Models\BoletimEpidemiologico;
use App\Models\Municipio;
use Illuminate\Database\Seeder;

class BoletimEpidemiologicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $quantidadeSuspeitos = 30;
        $quantidadeConfirmados = 20;
        $quantidadeObitos = 5;
        $quantidadeDescartados = 20;
        $quantidadeCurados = 7;
        $quantidadeObitosSuspeitos = 10;
        $quantidade_confirmados_previsto_fiocruz = 90;
        $quantidade_testagem = 30;
        $quantidade_leitos_uti = 10;
        $quantidade_leitos_uti_ocupados = 5;
        $indice_isolamento_social = 20;

        $incrementadorDiario = 10;

        $multiplicadorDiario = 0;

        $idMunicipioBauru = Municipio::isBauru()->first()->id;

        for ($dias = 0; $dias < 31; ++$dias) {
            $date = \Carbon\Carbon::now()->addDays(-$dias)->toDateString();

            if ($dias >= 15) {
                $incrementadorDiario = (abs($incrementadorDiario) * -1);
                --$multiplicadorDiario;
            } else {
                ++$multiplicadorDiario;
            }
            $quantidadeSuspeitos = $quantidadeSuspeitos + ($multiplicadorDiario * $incrementadorDiario);
            $quantidadeConfirmados = $quantidadeConfirmados + ($multiplicadorDiario * $incrementadorDiario);
            $quantidadeObitos = $quantidadeObitos + ($multiplicadorDiario * $incrementadorDiario);
            $quantidadeDescartados = $quantidadeDescartados + ($multiplicadorDiario * $incrementadorDiario);
            $quantidadeCurados = $quantidadeCurados + ($multiplicadorDiario * $incrementadorDiario);
            $quantidadeObitosSuspeitos = $quantidadeObitosSuspeitos + ($multiplicadorDiario * $incrementadorDiario);
            $quantidade_confirmados_previsto_fiocruz = $quantidade_confirmados_previsto_fiocruz + ($multiplicadorDiario * $incrementadorDiario);
            $quantidade_testagem = $quantidade_testagem + ($multiplicadorDiario * $incrementadorDiario);
            $quantidade_leitos_uti = $quantidade_leitos_uti + ($multiplicadorDiario * $incrementadorDiario);
            $quantidade_leitos_uti_ocupados = $quantidade_leitos_uti_ocupados + ($multiplicadorDiario * $incrementadorDiario);
            $indice_isolamento_social = abs(round($indice_isolamento_social + ($multiplicadorDiario * $incrementadorDiario) / ($indice_isolamento_social <= 0 ? 1 : $indice_isolamento_social), 2));
            $indice_isolamento_social = $indice_isolamento_social > 100 ? 100 : $indice_isolamento_social;


            BoletimEpidemiologico::updateOrCreate([
                'data' => $date,
                'id_municipio' => $idMunicipioBauru,
            ], [
                'data' => $date,
                'quantidade_suspeitos' => $quantidadeSuspeitos,
                'quantidade_confirmados' => $quantidadeConfirmados,
                'quantidade_obitos' => $quantidadeObitos,
                'id_municipio' => $idMunicipioBauru,
                'quantidade_descartados' => $quantidadeDescartados,
                'quantidade_curados' => $quantidadeCurados,
                'quantidade_obitos_suspeitos' => $quantidadeObitosSuspeitos,
                'quantidade_confirmados_previsto_fiocruz' => $quantidade_confirmados_previsto_fiocruz,
                'quantidade_testagem' => $quantidade_testagem,
                'quantidade_leitos_uti' => $quantidade_leitos_uti,
                'quantidade_leitos_uti_ocupados' => $quantidade_leitos_uti_ocupados,
                'indice_isolamento_social' => $indice_isolamento_social,
            ]);
        }
    }
}
