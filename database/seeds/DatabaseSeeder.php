<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        // $this->call(RoleSeeder::class);
        // $this->call(UsuarioSeeder::class);
        $this->call(PacienteBauruSeeder::class);
        $this->call(BoletimEpidemiologicoSeeder::class);
        $this->call(ChatSeeder::class);
    }
}
