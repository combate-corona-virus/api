<?php

use App\Imports\ImportPacienteSeeder;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class PacienteBauruSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $pathJsonBauru = storage_path('importacao/paciente_bauru.csv');

        Excel::import(new ImportPacienteSeeder(), $pathJsonBauru);
    }
}
