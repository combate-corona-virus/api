<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Role::insert(
            [
                [
                'name' => 'supervisor',
                'display_name' => 'Supervisor',
                'description' => 'Supervisor Covid',
                ],
                [
                'name' => 'tecnico',
                'display_name' => 'Técnico',
                'description' => 'Técnico Covid',
                ],
            ]);
    }
}
