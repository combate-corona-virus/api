<?php

use App\Models\Municipio;
use App\Models\Role;
use App\Models\Usuario;
use Illuminate\Database\Seeder;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $bauru = Municipio::isBauru()->first();

        Role::all()->each(function ($role) use ($bauru) {
            Usuario::create(
                [
                    'nome' => $role->display_name,
                    'sobrenome' => 'Sobrenome: ' . $role->description,
                    'email' => $role->name . '@combatecoronavirus.com.br',
                    'password' => '123mudar',
                    'username' => $role->name . '@combatecoronavirus.com.br',
                    'id_municipio' => $bauru->id,
                ]
            )->attachRole($role);
        });
    }
}
