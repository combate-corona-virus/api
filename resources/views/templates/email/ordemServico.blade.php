<body style="margin: 0; padding: 0; background-color: #f6f6f6;">
    <section style="max-width: 600px;margin: 10px auto;border-radius: 20px;overflow: hidden;box-shadow: 1px 1px 2px rgba(0,0,0,0.1);background: #ffffff; border: 1px solid #e0e0e0;">
        <header style="background-color: #393f44; background-position: top right;background-size: contain;background-repeat: no-repeat;display: inline-block;width: 100%;">
            <a href="{{ $url }}" style="color: #393f44">
                <img src="https://s3-sa-east-1.amazonaws.com/storage.cpos/somasig.logo.png" style="background-color: #01B564; padding: 40px; border-radius: 0 0 30px 30px; float: left; margin: 0 25px;">

                <h1 style="font-family: Arial, Helvetica, sans-serif;margin: 0 0 15px 0;font-size: 24px;color: #01B564;padding: 20px 55px 0 0;"> Ordem de serviço
                    <span style="display: block;font-size: 18px;color: #01B564;font-weight: 100;margin-top: 5px;"> n&deg; {{ $id }} </span>
                </h1>
            <h2 style="font-family: Arial, Helvetica, sans-serif;color: #01B564;font-size: 20px;background: #ffffff;border-top: 3px solid #01B564;padding: 15px;margin-bottom: 0;">
                <span style="display: block;font-size: 16px;font-weight: 100;"></span>
            </h2>
        </a>
        </header>
        <article style="background: #fff;padding: 15px 30px;box-sizing: border-box;display: inline-block;width: 100%;">
            <h3 style="font-family: Arial, Helvetica, sans-serif;color: #393f44;margin: 25px 0 40px 0;font-size: 24px;">
                <span style="background: #393f44;width: 20px;height: 40px;display: inline-block;margin-left: -30px;vertical-align: middle;border-radius: 0 8px 8px 0;margin-right: 10px;"></span>{{ $nomeDestinatario }},</h3>
            A ordem de serviço <strong> n&deg; {{ $id }} </strong> se encontra no status  <a href="{{ $url }}"> <strong> {{ $status }} </strong> </a>.
        </article>

        <footer style="background: #ffffff;display: inline-block;width: 100%;padding: 15px 30px;box-sizing: border-box;border-top: 1px solid #e4e4e4;">
                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;color: #393f44;float: left;">
                    <a href="{{ $url }}">
                        clique aqui para visualizar a OS
                    </a>
                </p>
            <img src="https://s3-sa-east-1.amazonaws.com/storage.cpos/2000px-Gnome-network-server.svg.png" style="float: right;">
        </footer>
    </section>
</body>

