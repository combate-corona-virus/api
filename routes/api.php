<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->middleware('auth:api');

Route::group(['prefix' => 'municipio'], function () {
    Route::post('logo', 'MunicipioController@uploadLogo');
});

Route::group(['prefix' => 'arquivo'], function () {
    Route::group(['prefix' => '{id}'], function () {
        Route::get('download', 'ArquivoController@download')->name('arquivo.download');
    });
});

Route::group(['prefix' => 'paciente'], function () {
    Route::group(['prefix' => 'importacao'], function () {
        Route::post('/', 'PacienteController@importPaciente')->middleware('auth:api');
        Route::get('/', 'PacienteController@downloadTemplate');
    });
});
